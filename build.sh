#!/usr/bin/env bash
REQUIRED_LIB="translate-toolkit libssh-dev libgtk2.0-dev make g++ cmake openbox libxdamage-dev libxxf86vm-dev  libasound2-dev gksu pcmanfm oxygen-icon-theme libfreetype6-dev libfontconfig1-dev libglib2.0-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libice-dev libaudio-dev libgl1-mesa-dev  libc6-dev libsm-dev libxcursor-dev libxext-dev libxfixes-dev libxi-dev libxinerama-dev libxrandr-dev libxrender-dev libx11-dev ros-cmake-modules libtinyxml2-dev"
ORGANIZATION="mortaro"
DEBIAN_REVISION=1
MACHINE=$(uname -m)
if [ $MACHINE = "x86_64" ]; then
	ARCH="amd64"
else
	ARCH="i386"
fi

if [ -n "$1" ]; then
CONFIG_DIR=$1/.config
else
    CONFIG_DIR="${HOME}/.config"
fi
if [ "$(apt-cache pkgnames|grep copperspice)" = "" ]; then
    sudo dpkg -i copperspice-1.4.5-Linux-$ARCH.deb
fi
if [ ! -d build ]; then
    mkdir -v build
fi
cd build
sudo apt-get -y install ${REQUIRED_LIB}
sed -i.bak '/#define __DEBUG__/d' ../src/mydebug.h
cmake ../
make package
sudo apt-get -y purge qtpanel*
sudo dpkg -i qtpanel_*.deb
cd ..
mkdir -vp $CONFIG_DIR/$ORGANIZATION
if [ ! -e "${CONFIG_DIR}/${ORGANIZATION}/QtPanel.conf" ]; then
    cp -v data/QtPanel.conf ${CONFIG_DIR}/$ORGANIZATION
fi
if [ ! -e "${CONFIG_DIR}/${ORGANIZATION}/applets.xml" ]; then
    cp -v data/applets.xml ${CONFIG_DIR}/$ORGANIZATION
fi
if [ ! -f "$CONFIG_DIR/openbox/autostart" ]; then
    mkdir -v -p $CONFIG_DIR/openbox
    touch $CONFIG_DIR/openbox/autostart
fi
TEMP1=$(grep QtPanel ${CONFIG_DIR}/openbox/autostart)
TEMP2=$(grep pcmanfm ${CONFIG_DIR}/openbox/autostart)
if [ "$TEMP1" = "" ]; then
    echo "QtPanel &" >> $CONFIG_DIR/openbox/autostart
fi
if [ "$TEMP2" = "" ]; then
    echo "pcmanfm --desktop &" >> $CONFIG_DIR/openbox/autostart
fi
#rm -v -R -f build
rm -vf build/*.deb
if [ "$1" = "-purge" ]; then
    sudo apt-get purge -y cmake g++ libx11-dev libxxf86vm-dev libxrender-dev libxcomposite-dev libasound2-dev
    sudo apt-get -y --purge autoremove
fi

cp -vf src/mydebug.h.bak src/mydebug.h
rm src/mydebug.h.bak
