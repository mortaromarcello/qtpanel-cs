#ifndef RUNPROCESS_H
#define RUNPROCESS_H

#include <QDialog>
#include <QProcess>
#include <QDebug>
#include <QPushButton>

#include "q_debugstream.h"

class RunProcess: public QDialog
{
	CS_OBJECT(RunProcess)

public:
	RunProcess(QProcess *proc, QWidget * parent = 0, Qt::WindowFlags flags = 0);
	~RunProcess();

private :
	CS_SLOT_1(Private, void processOutput())
	CS_SLOT_2(processOutput) 
	CS_SLOT_1(Private, void finished(int exitCode,QProcess::ExitStatus status))
	CS_SLOT_2(finished) 
	CS_SLOT_1(Private, void abort())
	CS_SLOT_2(abort) 

private:
	Q_DebugStream *m_stream;
	QProcess* m_proc;
	QTextEdit* m_output;
	QPushButton* m_ok;
	QPushButton* m_cancel;
};

#endif
