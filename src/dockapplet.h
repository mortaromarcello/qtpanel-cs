#ifndef DOCKAPPLET_H
#define DOCKAPPLET_H

#include <QVector>
#include <QMap>
#include <QIcon>
#include <QGraphicsItem>
#include "applet.h"

class QGraphicsPixmapItem;
class TextGraphicsItem;
class DockApplet;
class Client;
class XfitMan;
class QStringList;

// Item di una finestra. Deve avere un Client*.
class DockItem: public QObject, public QGraphicsItem
{
    CS_OBJECT(DockItem)
    CS_INTERFACES(QGraphicsItem)public:
    DockItem(DockApplet* dockApplet, Client* client);
    ~DockItem();

    void updateContent();
    void setTargetPosition(const QPoint& targetPosition);
    void setTargetSize(const QSize& targetSize);
    void moveInstantly();
    void startAnimation();
    // ritorna il puntatore al Client
    Client* client() {return m_client;}

    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);

public :
    CS_SLOT_1(Public, void animate())
    CS_SLOT_2(animate) 
    CS_SLOT_1(Public, void close())
    CS_SLOT_2(close) 
    CS_SLOT_1(Public, void maximize())
    CS_SLOT_2(maximize) 
    CS_SLOT_1(Public, void demaximize())
    CS_SLOT_2(demaximize) 

protected:
    void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);

private:
    void updateClientIconGeometry();
    bool isUrgent();

    QTimer* m_animationTimer;
    DockApplet* m_dockApplet;
    TextGraphicsItem* m_textItem;
    QGraphicsPixmapItem* m_iconItem;
    Client* m_client;
    QPoint m_position;
    QPoint m_targetPosition;
    QSize m_size;
    QSize m_targetSize;
    qreal m_highlightIntensity;
    qreal m_urgencyHighlightIntensity;
    bool m_dragging;
    QPointF m_mouseDownPosition;
    QPoint m_dragStartPosition;
};

/*
// Used for tracking connected windows (X11 clients).
// Client may have it's DockItem, but not necessary (for example, special windows are not shown in dock).
class Client
{
public:
    Client(unsigned long handle);
    ~Client();
    // ritorna l'handle alla finestra
    unsigned long handle() const {return m_handle;}
    pid_t pid();
    // è visibile?
    bool isVisible() {return m_visible;}
    // ritorna il nome della finestra
    const QString& name() const {return m_name;}
    // ritorna il path dell programma
    const QString& path() const {return m_path;}
    // ritorna l'icona della finestra;
    const QStringList& cmdline() {return m_cmdline;}
    const QIcon& icon() const {return m_icon;}
    // è urgente?
    bool isUrgent() const {return m_isUrgent;}
    // è attiva?
    bool isActive();
    bool isHidden();
    bool isAboveLayer();
    // ritorna il desktop della finestra
    int getDesktop();
    // attiva la finestra
    void activate();
    void minimize();
    void maximize();
    void demaximize();
    void close();
    // gestisce i messaggi alla finestra
    void windowPropertyChanged(unsigned long atom);

private:
    void updateVisibility();
    void updateName();
    void updateIcon();
    void updateUrgency();
    unsigned long m_handle;
    pid_t m_pid;
    QString m_path;
    QString m_name;
    QStringList m_cmdline;
    QIcon m_icon;
    bool m_isUrgent;
    bool m_visible;
};
*/

class DockApplet: public Applet
{
    CS_OBJECT(DockApplet)
public:
    DockApplet(PanelWindow* panelWindow);
    ~DockApplet();

    bool init();
    QSize desiredSize();
    void registerDockItem(DockItem* dockItem);
    void unregisterDockItem(DockItem* dockItem);
    void updateLayout();
    void draggingStarted();
    void draggingStopped();
    void moveItem(DockItem* dockItem, bool right);
    void xmlWrite(XmlConfigWriter* writer);

protected :
    CS_SLOT_1(Protected, void layoutChanged())
    CS_SLOT_2(layoutChanged) 

private :
    CS_SLOT_1(Private, void windowPropertyChanged(unsigned long window,unsigned long atom))
    CS_SLOT_2(windowPropertyChanged) 

private:
    // aggiorna la lista delle finestre
    void updateClientList();
    // ritorna il puntatore al client della lista oppure nullptr se non c'è
    Client* getClientFromDockItems(unsigned long handle);
    DockItem* getDockItem(unsigned long handle);
    // lista delle dockitem
    QVector<DockItem*> m_dockItems;
    bool m_dragging;
};

#endif
