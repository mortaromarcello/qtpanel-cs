extern Display *dpy;

inline void releasePicture( Picture &picture )
{
    if ( picture ) {
        XRenderFreePicture( dpy, picture );
        picture = None;
    }
}

inline void releasePixmap( Pixmap &pixmap )
{
    if ( pixmap ) {
        XFreePixmap( dpy, pixmap );
        pixmap = None;
    }
}

inline void releaseRegion( XserverRegion &region )
{
    if ( region ) {
        XFixesDestroyRegion( dpy, region );
        region = None;
    }
}

inline XserverRegion createRegionFromQRect( const QRect &r )
{
    XRectangle rect = { (short int)r.x(), (short int)r.y(), (short unsigned int)r.width(), (short unsigned int)r.height() };
    return XFixesCreateRegion( dpy, &rect, 1 );
}
