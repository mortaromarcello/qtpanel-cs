#ifndef CLIENT_H
#define CLIENT_H

#include <QString>
#include <QStringList>
#include <QIcon>

// Used for tracking connected windows (X11 clients).
// Client may have it's DockItem, but not necessary (for example, special windows are not shown in dock).
class Client
{
public:
    Client(unsigned long handle);
    ~Client();
    // ritorna l'handle alla finestra
    unsigned long handle() const {return m_handle;}
    pid_t pid();
    // è visibile?
    bool isVisible() {return m_visible;}
    // ritorna il nome della finestra
    const QString& name() const {return m_name;}
    // ritorna il path dell programma
    const QString& path() const {return m_path;}
    // ritorna l'icona della finestra;
    const QStringList& cmdline() {return m_cmdline;}
    const QIcon& icon() const {return m_icon;}
    // è urgente?
    bool isUrgent() const {return m_isUrgent;}
    // è attiva?
    bool isActive();
    bool isHidden();
    bool isAboveLayer();
    // ritorna il desktop della finestra
    int getDesktop();
    // attiva la finestra
    void activate();
    void minimize();
    void maximize();
    void demaximize();
    void close();
    // gestisce i messaggi alla finestra
    void windowPropertyChanged(unsigned long atom);

private:
    void updateVisibility();
    void updateName();
    void updateIcon();
    void updateUrgency();
    unsigned long m_handle;
    pid_t m_pid;
    QString m_path;
    QString m_name;
    QStringList m_cmdline;
    QIcon m_icon;
    bool m_isUrgent;
    bool m_visible;
};

#endif // CLIENT_H
