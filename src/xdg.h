#ifndef XDG_H
#define XDG_H

#include <QString>
#include <QProcessEnvironment>

#define XDG_DATA_HOME_DEFAULT   ".local/share"
#define XDG_CONFIG_HOME_DEFAULT ".config"
#define XDG_CACHE_HOME_DEFAULT  ".cache"
#define XDG_DATA_DIRS_DEFAULT   "/usr/local/share/:/usr/share/"
#define XDG_CONFIG_DIRS_DEFAULT "/etc/xdg"
#define XDG_MENU_PREFIX_DEFAULT "qtpanelcs-"

class XdgDirectory
{
public:
    XdgDirectory();
    ~XdgDirectory();
    QProcessEnvironment getEnvironment() {return m_environment;}
    QString getHome() {return m_home;}
    QString getXdgDataHome() {return m_xdg_data_home;}
    QString getXdgConfigHome() {return m_xdg_config_home;}
    QString getXdgCacheHome() {return m_xdg_cache_home;}
    QString getXdgDataDirs() {return m_xdg_data_dirs;}
    QStringList getXdgDataDirsList() {return m_xdg_data_dirs.split(":");}
    QString getXdgConfigDirs() {return m_xdg_config_dirs;}
    QStringList getXdgConfigDirsList() {return m_xdg_config_dirs.split(":");}
    QString getXdgAutostartHome() {return m_xdg_autostart_home;}
    QString getXdgAutostartDirs() {return m_xdg_autostart_dirs;}
    QString getXdgTrashHome() {return m_xdg_trash_home;}

private:
    QProcessEnvironment m_environment;
    QString m_home;
    QString m_xdg_data_home;
    QString m_xdg_config_home;
    QString m_xdg_cache_home;
    QString m_xdg_data_dirs;
    QString m_xdg_config_dirs;
    QString m_xdg_autostart_home;
    QString m_xdg_autostart_dirs;
    QString m_xdg_trash_home;
    
};

#endif // XDG_H
