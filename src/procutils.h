#ifndef PROCUTILS_H
#define PROCUTILS_H

pid_t getProcIdByName(std::string procName);
std::list<std::string> getCmdLineByPid(pid_t pid);

#endif // PROCUTILS_H
