#include "runprocess.h"
#include "mydebug.h"
#include <QLayout>
#include <QMessageBox>

RunProcess::RunProcess(QProcess* proc, QWidget * parent, Qt::WindowFlags flags):
	QDialog(parent, flags)
{
	if (proc == nullptr)
		return;
	m_proc = proc;
	m_ok = new QPushButton(QIcon::fromTheme("ok"), "Ok");
	m_ok->setEnabled(false);
	m_cancel = new QPushButton(QIcon::fromTheme("cancel"), "Cancel");
	m_output = new QTextEdit();
	connect (m_proc, SIGNAL(readyReadStandardOutput()), this, SLOT(processOutput()));
	connect (m_proc, SIGNAL(readyReadStandardError()), this, SLOT(processOutput()));
	connect(m_proc, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(finished(int, QProcess::ExitStatus)));
	connect(m_ok, SIGNAL(clicked()), this, SLOT(accept()));
	connect(m_cancel, SIGNAL(clicked()), this, SLOT(reject()));
	connect(m_cancel, SIGNAL(clicked()), this, SLOT(abort()));
	m_stream = new Q_DebugStream(std::cout, m_output);
	Q_DebugStream::registerQDebugMessageHandler();
	QGridLayout* mainGrid = new QGridLayout();
	QVBoxLayout* topLayout = new QVBoxLayout();
	topLayout->addWidget(m_output);
	mainGrid->addLayout(topLayout, 0, 0);
	QHBoxLayout* hLayout = new QHBoxLayout();
	hLayout->addWidget(m_ok);
	hLayout->addWidget(m_cancel);
	mainGrid->addLayout(hLayout, 1, 0);
	setLayout(mainGrid);
}

RunProcess::~RunProcess()
{
	if (m_proc->state() == QProcess::Running)
			m_proc->waitForFinished(-1);
	delete m_stream;
}

void RunProcess::processOutput() {
	qDebug() << m_proc->readAllStandardOutput();
}

void RunProcess::finished(int exitCode, QProcess::ExitStatus status)
{
	if (status == QProcess::CrashExit || exitCode)
		QMessageBox::warning(this, tr("Warning"), tr("Compilation Error!"));
	m_ok->setEnabled(true);
	m_cancel->setEnabled(false);
	qDebug() << "\n\n!!!FINISHED!!!\n\n";
}

void RunProcess::abort()
{
	m_proc->kill();
	reject();
}
