#include "mediaapplet2.h"
#include "qtpanelconfig.h"

#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QDir>
#include <QFileDialog>
#include <QKeyEvent>
#include <QStyle>

#include "panelwindow.h"
#include "panelapplication.h"
#include "mediaplayer.h"

MediaButton::MediaButton(const QString& icon, MediaPlayer* mediaplayer, QGraphicsItem * parent):
    QGraphicsPixmapItem(parent), m_mediaPlayer(mediaplayer), m_useInternalIcon(true)
{
    setIcon(icon);
    setIconPixmap(QIcon::Disabled);
}

void MediaButton::setIcon(const QString& icon)
{
    if (m_useInternalIcon) {
        m_icon = QIcon(":/images/" + icon);
    }
    else {
        if (QIcon::hasThemeIcon(QFileInfo(icon).baseName()))
         m_icon = QIcon::fromTheme(QFileInfo(icon).baseName());
         else
            m_icon = QIcon(":/images/" + icon);
    }
}

void MediaButton::setIconPixmap(QIcon::Mode mode, int extent)
{
    setPixmap(m_icon.pixmap(extent, mode));
}

void MediaButtonPlayPause::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if (event->buttons() == Qt::LeftButton) {
        if (isPlaying())
            pause();
        if (isPaused() || isStopped())
            play();
    }
}

void MediaButtonPlayPause::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{}

void MediaButtonStop::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if (event->buttons() == Qt::LeftButton) {
        stop();
    }
}

void MediaButtonStop::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{}

MediaApplet2::MediaApplet2(PanelWindow* panelWindow)
    : Applet(panelWindow),
    m_currentPath(QDir::homePath()),                            // path corrente
    m_fileMenu(new QMenu())                                     // menù file
{
    m_player = new MediaPlayer();                               //
    m_open = new QGraphicsPixmapItem(this);
    m_open->setPixmap(QIcon::fromTheme("fileopen").pixmap(24));
    m_open->setOffset(0, 4);
    m_prev = new MediaButtonPrev(m_player, this);
    m_prev->setOffset(24 + 2, 4);
    m_playPause = new MediaButtonPlayPause(m_player, this);     // icona playPause
    m_playPause->setOffset(48 + 2, 4);
    m_stop = new MediaButtonStop(m_player, this);                   // icona stop
    m_stop->setOffset(72 + 2, 4);
    m_next = new MediaButtonNext(m_player, this);
    m_next->setOffset(96 + 2, 4);
    connect(m_player->mediaObject(), SIGNAL(stateChanged(Phonon::State, Phonon::State)), m_playPause, SLOT(enableButton(Phonon::State, Phonon::State)));
    connect(m_player->mediaObject(), SIGNAL(stateChanged(Phonon::State, Phonon::State)), m_stop, SLOT(enableButton(Phonon::State, Phonon::State)));
    // segnale di hasVideo  (indica se il media ha lo stream video) cambiato
    connect(m_player->mediaObject(), SIGNAL(hasVideoChanged(bool)), this, SLOT(hasVideoChanged(bool)));
    createFileMenu();
}

MediaApplet2::~MediaApplet2()
{
    delete m_playPause;
    delete m_stop;
    delete m_player;
    delete m_fileMenu;
}

bool MediaApplet2::init()
{
    setInteractive(true);
    return true;
}

void MediaApplet2::showConfigurationDialog() {}

void MediaApplet2::openFile()
{
    QString fileName = QFileDialog::getOpenFileName(m_player, tr("Open Media"), m_currentPath, tr("Audio/Video (*.mp3 *.ogg *.avi *.mp4 *.mkv *.webm);;All (*.*)"));
    if (fileName.isEmpty())
        return;
    MyDBG(fileName);
    m_player->loadFile(fileName);
    m_player->show();
    m_player->play();
    m_currentPath = QDir(m_player->currentFile()).path();
}

void MediaApplet2::createFileMenu()
{
    m_openFileAction = m_fileMenu->addAction(QIcon::fromTheme("stock_open"), tr("Open &Files..."), this, SLOT(openFile()));
}

QSize MediaApplet2::desiredSize()
{
    return QSize(102 + 24, 64);
}

void MediaApplet2::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
}

void MediaApplet2::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if (event->buttons() == Qt::LeftButton) {
        m_fileMenu->exec(localToScreen(QPoint(0, 0)));
    }
}

void MediaApplet2::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
}

void MediaApplet2::stateChanged(Phonon::State newstate, Phonon::State oldstate)
{
    switch(newstate) {
        case Phonon::ErrorState:
            MyDBG("Error State:" << m_player->mediaObject()->errorType() << m_player->mediaObject()->errorString());
            break;
    }
}

void MediaApplet2::hasVideoChanged(bool changed)
{

    MyDBG("hasVideo changed:" << changed);

    if (changed) {
        m_player->resize(m_player->videoWidget()->sizeHint());
        m_player->show();
    }
    if (!changed)
        m_player->hide();
}
