#include "client.h"
#include "xfitman.h"
#include "procutils.h"
#include "mydebug.h"

Client::Client(unsigned long handle)
{
    m_handle = handle;
    m_pid = XfitMan::getWindowPid(handle);
    m_path = XfitMan::getExeForPid(m_pid);
    m_name = XfitMan::getWindowTitle(m_handle);
    if (!XfitMan::getClientIcon(m_handle, &m_icon))
        m_icon = QIcon();
    m_isUrgent = XfitMan::getWindowUrgency(m_handle);
    XfitMan::registerForWindowPropertyChanges(m_handle);
    std::list<std::string> cmdline = getCmdLineByPid(m_pid);
    for (std::string str: cmdline)
        m_cmdline << str.c_str();
    updateVisibility();
    activate();
    int currentDesktop = XfitMan::getActiveDesktop();
    if (currentDesktop != getDesktop())
        XfitMan::setActiveDesktop(getDesktop());
}

Client::~Client()
{
}

pid_t Client::pid()
{
    XfitMan::getWindowPid(handle());
}

void Client::windowPropertyChanged(unsigned long atom)
{
    if(atom == XfitMan::atom("_NET_WM_WINDOW_TYPE") || atom == XfitMan::atom("_NET_WM_STATE")) {

        MyDBG(QString("atom:%1").arg(atom == XfitMan::atom("_NET_WM_WINDOW_TYPE")?"_NET_WM_WINDOW_TYPE":"_NET_WM_STATE"));

        updateVisibility();
    }
    if(atom == XfitMan::atom("_NET_WM_VISIBLE_NAME") || atom == XfitMan::atom("_NET_WM_NAME") || atom == XfitMan::atom("WM_NAME")) {
        updateName();

        MyDBG("atom:_NET_WM_VISIBLE_NAME || _NET_WM_NAME || WM_NAME ->" << name());

    }
    if(atom == XfitMan::atom("_NET_WM_ICON")) {

        MyDBG("atom:_NET_WM_ICON");

        updateIcon();
    }
    if(atom == XfitMan::atom("WM_HINTS")) {

    MyDBG("atom:WM_HINTS");

        updateUrgency();
    }
    if(atom == XfitMan::atom("_NET_WM_ACTIVE_WINDOW")) {

        MyDBG("atom:_NET_WM_ACTIVE_WINDOW");

    }
}

void Client::updateVisibility()
{
    AtomList windowTypes = XfitMan::getWindowType(handle());
    WindowState state = XfitMan::getWindowState(handle());
    m_visible = (windowTypes.size() == 0) || (windowTypes.size() == 1 && windowTypes[0] == XfitMan::atom("_NET_WM_WINDOW_TYPE_NORMAL"));
    if(state.SkipTaskBar)
        m_visible = false;
}

void Client::updateName()
{
    m_name = XfitMan::getWindowTitle(handle());
}

void Client::updateIcon()
{
    if (!XfitMan::getClientIcon(handle(), &m_icon))
        m_icon = QIcon();
}

void Client::updateUrgency()
{
    m_isUrgent = XfitMan::getWindowUrgency(handle());
}

bool Client::isActive()
{

    MyDBG("isActive:" << handle() << "->" << XfitMan::getActiveWindow());

    WindowState state = XfitMan::getWindowState(handle());
    MyDBG(state.Focused);
    return (handle() == XfitMan::getActiveWindow());
}

bool Client::isHidden()
{
    WindowState state;
    state = XfitMan::getWindowState(handle());
    return state.Hidden;
}

bool Client::isAboveLayer()
{
    WindowState state;
    state = XfitMan::getWindowState(handle());
    return state.AboveLayer;
}

int Client::getDesktop()
{
    return XfitMan::getWindowDesktop(handle());
}

void Client::activate()
{
    XfitMan::raiseWindow(handle());
}

void Client::minimize()
{
    XfitMan::minimizeWindow(handle());
}

void Client::maximize()
{
    XfitMan::maximizeWindow(handle(), XfitMan::MaximizeBoth);
}

void Client::demaximize()
{
    XfitMan::deMaximizeWindow(handle());
}

void Client::close()
{
    XfitMan::closeWindow(handle());
}
