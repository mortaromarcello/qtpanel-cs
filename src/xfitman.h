/* BEGIN_COMMON_COPYRIGHT_HEADER
 * (c)LGPL2+
 *
 * Razor - a lightweight, Qt based, desktop toolset
 * http://razor-qt.org
 *
 * Copyright: 2010-2011 Razor team
 * Authors:
 *   Christopher "VdoP" Regali
 *   Alexander Sokoloff <sokoloff.a@gmail.com>
 *
 * This program or library is free software; you can redistribute it
 * and/or modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA
 *
 * END_COMMON_COPYRIGHT_HEADER */

#ifndef XFITMAN_H
#define XFITMAN_H

#include <QList>
#include <QPixmap>
#include <QString>
#include <QMap>
#include <QIcon>

//some net_wm state-operations we need here
#define _NET_WM_STATE_TOGGLE 2
#define _NET_WM_STATE_ADD 1
#define _NET_WM_STATE_REMOVE 0

/**
 * @file xfitman.h
 * @author Christopher "VdoP" Regali
 * @brief handles all of our xlib-calls.
 */

typedef unsigned long XID;
typedef XID Window;
typedef XID Pixmap;
typedef unsigned long Atom;
typedef QList<Atom> AtomList;
typedef QList<Window> WindowList;

// A list of atoms indicating user operations that the Window Manager supports
// for this window.
// See http://standards.freedesktop.org/wm-spec/latest/ar01s05.html#id2569373
struct WindowAllowedActions
{
    bool Move : 1;          // indicates that the window may be moved around the screen.
    bool Resize : 1;        // indicates that the window may be resized.
    bool Minimize : 1;      // indicates that the window may be iconified.
    bool Shade : 1;         // indicates that the window may be shaded.
    bool Stick : 1;         // indicates that the window may have its sticky state toggled.
    bool MaximizeHoriz : 1; // indicates that the window may be maximized horizontally.
    bool MaximizeVert : 1;  // indicates that the window may be maximized vertically.
    bool FullScreen : 1;    // indicates that the window may be brought to fullscreen state.
    bool ChangeDesktop : 1; // indicates that the window may be moved between desktops.
    bool Close : 1;         // indicates that the window may be closed.
    bool AboveLayer : 1;    // indicates that the window may placed in the "above" layer of windows
    bool BelowLayer : 1;    // indicates that the window may placed in the "below" layer of windows
};

// A list of hints describing the window state.
// http://standards.freedesktop.org/wm-spec/latest/ar01s05.html#id2569140
struct WindowState
{
    bool Modal : 1;         // indicates that this is a modal dialog box.
    bool Sticky : 1;        // indicates that the Window Manager SHOULD keep the window's position
                        // fixed on the screen, even when the virtual desktop scrolls.
    bool MaximizedVert : 1; // indicates that the window is vertically maximized.
    bool MaximizedHoriz : 1;// indicates that the window is horizontally maximized.
    bool Shaded : 1;        // indicates that the window is shaded.
    bool SkipTaskBar : 1;   // indicates that the window should not be included on a taskbar.
    bool SkipPager : 1;     // indicates that the window should not be included on a Pager.
    bool Hidden : 1;        // indicates that a window would not be visible on the screen
    bool FullScreen : 1;    // indicates that the window should fill the entire screen.
    bool AboveLayer : 1;    // indicates that the window should be on top of most windows.
    bool BelowLayer : 1;    // indicates that the window should be below most windows.
    bool Attention : 1;     // indicates that some action in or with the window happened.
    bool Focused : 1;
};


/**
 * @brief manages the Xlib apicalls
 */
class XfitMan: public QObject
{
    CS_OBJECT(XfitMan)

public:

    enum Layer
    {
        LayerAbove,
        LayerNormal,
        LayerBelow
    };

    enum MaximizeDirection
    {
        MaximizeHoriz,
        MaximizeVert,
        MaximizeBoth
    };

    enum WMState
    {
        WMStateWithdrawn = 0,
        WMStateNormal = 1,
        WMStateIconic = 3
    };

    ~XfitMan();
    XfitMan();

    // See
    void setStrut(Window _wid,
                  int left, int right,
                  int top,  int bottom,

                  int leftStartY,   int leftEndY,
                  int rightStartY,  int rightEndY,
                  int topStartX,    int topEndX,
                  int bottomStartX, int bottomEndX
                  ) const;
    static WindowList getClientList();
    WindowList getClientListStacking() const;
    static bool getClientIcon(Window _wid, QPixmap& _pixreturn);
    static bool getClientIcon(Window _wid, QIcon *icon);
    //**********
    static QString getExeForPid(pid_t pid);
    static pid_t getWindowPid(Window _wid);
    //**********
    static int getWindowDesktop(Window _wid);
    static void moveWindowToDesktop(Window _wid, int _display);
    static void raiseWindow(Window _wid);
    static void minimizeWindow(Window _wid);
    static void maximizeWindow(Window _wid, MaximizeDirection direction = MaximizeBoth);
    static void deMaximizeWindow(Window _wid);
    void shadeWindow(Window _wid, bool shade) const;
    static void closeWindow(Window _wid);
    void setWindowLayer(Window _wid, Layer layer) const;

    static void setActiveDesktop(int _desktop);
    bool isHidden(Window _wid) const;
    WindowAllowedActions getAllowedActions(Window window) const;
    static WindowState getWindowState(Window window);
    static int getActiveDesktop();
    static Window getActiveAppWindow();
    static Window getActiveWindow();
    static int getNumDesktop();
    bool getShowingDesktop() const;
    void setShowingDesktop(bool show) const;

    static void setIconGeometry(Window _wid, QRect* rect = 0);
//
    static Pixmap getRootPixmap();
    void onX11Event(XEvent *event);
//
    /*!
     * Returns ICCCM WM_STATE
     */
    static WMState getWMState(Window _wid);

    /*!
     * Returns the names of all virtual desktops. This is a list of UTF-8 encoding strings.
     *
     * Note: The number of names could be different from getNumDesktop(). If it is less
     * than getNumDesktop(), then the desktops with high numbers are unnamed. If it is
     * larger than getNumDesktop(), then the excess names outside of the getNumDesktop()
     * are considered to be reserved in case the number of desktops is increased.
     */
    QStringList getDesktopNames() const;

    /*!
     * Returns the name of virtual desktop.
     */
    QString getDesktopName(int desktopNum, const QString &defaultName=QString()) const;

    /*! Returns window title if available
     *
     */
    static QString getWindowTitle(Window _wid);

    /*! Returns window title if available
     *
     */
    QString getApplicationName(Window _wid) const;

    static bool acceptWindow(Window _wid);
//
    static bool getWindowUrgency(Window _wid);
// static
    static void moveWindow(Window _win, int _x, int _y);
    static void resizeWindow(Window _wid, int _width, int _height);
    static void registerForWindowPropertyChanges(Window _wid);
    static XfitMan* instance() {return m_instance;}
    static void setWindowBackgroundBlack(Window _wid);
    static void registerForTrayIconUpdates(Window _wid);
    static void reparentWindow(Window _wid, Window parent);
    static void redirectWindow(Window _wid);
    static void mapWindow(Window _wid);
    static Pixmap getWindowPixmap(Window _wid);
    static Window getRootWindow();
    static void freeSystemTray();
    static Atom systemTrayAtom();
    static bool makeSystemTray(Window _wid);
    static void setWindowPropertyVisualId(Window _wid, const char* name, unsigned long value);
    static unsigned long getARGBVisualId();
    static void removeWindowProperty(Window _wid, const char* name);
    static void setWindowPropertyCardinal(Window _wid, const char* name, unsigned long value);
    static void setWindowPropertyCardinalArray(Window _wid, const char* name, const QVector<unsigned long>& values);
    static void setWindowPropertyAtom(Window _wid, const char* _internAtom, const char* _type);
#ifdef DEBUG
    static QString debugWindow(Window wnd);
#endif
    static Atom atom(const char* atomName);
//
    static AtomList getWindowType(Window window);

    /*!
     *   QDesktopWidget have a bug http://bugreports.qt.nokia.com/browse/QTBUG-18380
     *   This workaraund this problem.
     */
    const QRect availableGeometry(int screen = -1) const;

    /*!
     *   QDesktopWidget have a bug http://bugreports.qt.nokia.com/browse/QTBUG-18380
     *   This workaraund this problem.
     */
    const QRect availableGeometry(const QWidget *widget) const;

    /*!
     *   QDesktopWidget have a bug http://bugreports.qt.nokia.com/browse/QTBUG-18380
     *   This workaraund this problem.
     */
    const QRect availableGeometry(const QPoint &point) const;

    static int clientMessage(Window _wid, Atom _msg,
                      long unsigned int data0,
                      long unsigned int data1 = 0,
                      long unsigned int data2 = 0,
                      long unsigned int data3 = 0,
                      long unsigned int data4 = 0);

    /*!
     * Returns true if the Window Manager is running; otherwise returns false.
     */
    bool isWindowManagerActive() const;
//
//signals:
    CS_SIGNAL_1(Public, void windowClosed(unsigned long window))
    CS_SIGNAL_2(windowClosed, window)

    CS_SIGNAL_1(Public, void windowReconfigured(unsigned long window, int x, int y, int width, int height))
    CS_SIGNAL_2(windowReconfigured, window, x, y, width, height)

    CS_SIGNAL_1(Public, void windowDamaged(unsigned long window))
    CS_SIGNAL_2(windowDamaged, window)

    CS_SIGNAL_1(Public, void windowPropertyChanged(unsigned long window, unsigned long atom))
    CS_SIGNAL_2(windowPropertyChanged, window, atom)

    CS_SIGNAL_1(Public, void clientMessageReceived(unsigned long window, unsigned long atom, void* data))
    CS_SIGNAL_2(clientMessageReceived, window, atom, data)
//
private:

    /** \warning Do not forget to XFree(result) after data are processed!
    */
    static bool getWindowProperty(Window window,
                           Atom atom,               // property
                           Atom reqType,            // req_type
                           unsigned long* resultLen,// nitems_return
                           unsigned char** result   // prop_return
                          );

    /** \warning Do not forget to XFree(result) after data are processed!
    */
    static bool getRootWindowProperty(Atom atom,               // property
                               Atom reqType,            // req_type
                               unsigned long* resultLen,// nitems_return
                               unsigned char** result   // prop_return
                              );
    //
    static Atom getAtomByName(const QString& name);
    int m_damageEventBase;
    static XfitMan* m_instance;
    //
};

const XfitMan& xfitMan();


#endif
