#include "recentfilesapplet.h"
#include "menu.h"
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QDesktopServices>
#include <QFileSystemWatcher>
#include <QUrl>
#include <QProcess>
#include <QFileInfo>
#include <tinyxml2.h>
#include "textgraphicsitem.h"
#include "panelwindow.h"
#include "ui_appletdemosettings.h"
#include "panelapplication.h"

using namespace tinyxml2;

RecentDocument::RecentDocument(QString href, QString mtype, QList<BookmarkApplication> bapps)
        :bookmark_href(href), mime_type(mtype), bookmark_applications(bapps)
{
    QUrl url(bookmark_href);
    path = url.toDisplayString(QUrl::PreferLocalFile);
}

RecentDocument::~RecentDocument()
{
}

void RecentDocument::open()
{
    QString process = bookmark_applications[0].name;
    QStringList args;
    args << getPath();
    MyDBG(process << " " << args);
    QProcess::startDetached(process, args, getenv("HOME"));
}

RecentFilesApplet::RecentFilesApplet(PanelWindow* panelWindow)
    : Applet(panelWindow), m_numItemRecentFiles(25)
{
    if (!xmlRead()) {
        qWarning("Don't read configuration applet file.");
    }
    m_namefile = QDesktopServices::storageLocation(QDesktopServices::HomeLocation) + "/.local/share/recently-used.xbel";
    MyDBG(m_namefile);
    m_file = new QFile(m_namefile);
    m_watcher = new QFileSystemWatcher();
    m_watcher->addPath(m_namefile);
    m_textItem = new TextGraphicsItem(this);
    m_textItem->setColor(Qt::white);
    m_textItem->setFont(m_panelWindow->font());
    m_textItem->setText(tr("Recent docs"));
}

RecentFilesApplet::~RecentFilesApplet()
{
    delete m_file;
    delete m_watcher;
    delete m_textItem;
}

bool RecentFilesApplet::init()
{
    Menu* menu;
    setInteractive(true);
    menu = createMenu("/home/marcello/src/git/qtpanel-cs/xdg/menus/qtpanelcs-applications.menu");
    if(menu)
        menu->print();
    return true;
}

void RecentFilesApplet::xmlWrite(XmlConfigWriter* writer)
{
    writer->writeStartElement("config");
    writer->writeEndElement();
}

void RecentFilesApplet::showConfigurationDialog()
{
}

void RecentFilesApplet::showContextMenu(const QPoint& point)
{
    QMenu menu;
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure..."), this, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-desktop"), tr("Configure Panel"), PanelApplication::instance(), SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure applets"), m_panelWindow, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("remove"), tr("Remove applet"), this, SLOT(removeApplet()));
    menu.exec(point);
}

void RecentFilesApplet::layoutChanged()
{
    m_textItem->setPos(0, m_panelWindow->textBaseLine());

}

QSize RecentFilesApplet::desiredSize()
{
    return QSize(m_textItem->boundingRect().size().width(), m_textItem->boundingRect().size().height());
}

void RecentFilesApplet::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
}

void RecentFilesApplet::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    if(isUnderMouse()) {
        QList<RecentDocument> rDocs = getRecentDocuments();
        QMenu* menu = new QMenu();
        menu->setAttribute(Qt::WA_TranslucentBackground, true);
        menu->setFont(m_panelWindow->font());
        for (int i=0; i < m_numItemRecentFiles; i++) {
            RecentDocument r = rDocs[i];
            if (QFileInfo(r.getPath()).exists()) {
                QString mime_type = QString(r.getMimeType());
                MyDBG(mime_type);
                QAction* action = new QAction(QIcon::fromTheme(mime_type.replace('/', '-'), QIcon::fromTheme("unknown")), QFileInfo(r.getPath()).fileName(), this);
                action->setData(r.getPath());
                menu->addAction(action);
            }
        }
        menu->move(localToScreen(QPoint(0, m_size.height())));
        QAction* act = menu->exec();
        if (act) {
            QString process = "mimeopen";
            QStringList args;
            args << "-n" << act->data().toString();
            MyDBG(process << " " << args);
            QProcess::startDetached(process, args, getenv("HOME"));
        }
    }
}

bool RecentFilesApplet::xmlRead()
{
    if (!m_xmlConfigReader.xmlOpen()) {
        MyDBG("Error opening file.");
#ifndef __DEBUG__
        qDebug("Error opening file.");
#endif
        return false;
    }
    while (!m_xmlConfigReader.atEnd()) {
        if (m_xmlConfigReader.hasError()) {
            m_xmlConfigReader.xmlErrorString();
            m_xmlConfigReader.xmlClose();
            return false;
        }
        while (m_xmlConfigReader.readNextStartElement())
            if (m_xmlConfigReader.name() == "applet")
            while (m_xmlConfigReader.readNextStartElement())
                if (m_xmlConfigReader.name() == "type")
                    if (m_xmlConfigReader.readElementText() == getNameApplet())
                        while (m_xmlConfigReader.readNextStartElement())
                            if (m_xmlConfigReader.name() == "config")
                                while (m_xmlConfigReader.readNextStartElement())
                                    //if (m_xmlConfigReader.name() == "color")
                                    ;
                                    //    xmlReadColor();
    }
    m_xmlConfigReader.xmlClose();
    return true;
}

QList<RecentDocument> RecentFilesApplet::getRecentDocuments()
{
    XMLDocument xmlDoc;
    QList<RecentDocument> recentDocs;
    XMLError eResult = xmlDoc.LoadFile("/home/marcello/.local/share/recently-used.xbel");
    XMLElement* pRoot = xmlDoc.FirstChildElement("xbel");
    const char* version = pRoot->Attribute("version");
    if (pRoot == nullptr) return recentDocs;
    XMLElement* bookmark = pRoot->FirstChildElement("bookmark");
    
    while (bookmark) {
        if (bookmark == nullptr) return recentDocs;
        QString href = bookmark->Attribute("href");
        XMLElement* info = bookmark->FirstChildElement("info");
        
        XMLElement* metadata = info->FirstChildElement("metadata");
        XMLElement* mime = metadata->FirstChildElement("mime:mime-type");
        QString mtype = mime->Attribute("type");
        XMLElement* bookmark_applications = metadata->FirstChildElement("bookmark:applications");
        XMLElement* bookmark_application = bookmark_applications->FirstChildElement("bookmark:application");
        QList<BookmarkApplication> bapps;
        while (bookmark_application != nullptr) {
            struct BookmarkApplication bapp;
            bapp.name = bookmark_application->Attribute("name");
            bapp.exec = bookmark_application->Attribute("exec");
            bapps.push_back(bapp);
            bookmark_application = bookmark_application->NextSiblingElement("bookmark:application");
        }
        RecentDocument rDoc(href, mtype, bapps);
        recentDocs.push_front(rDoc);
        bookmark = bookmark->NextSiblingElement("bookmark");
    }
#ifdef __DEBUG__
    for (RecentDocument r: recentDocs)
    {
        MyDBG(r.getBookmarkHref());
        MyDBG(r.getPath() << " " << r.getMimeType());
        for(BookmarkApplication a: r.getBookmarkApplications())
        {
            MyDBG(a.name << " " << a.exec);
        }
    }
#endif
    return recentDocs;
}
