#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

#include <phonon>
#include <QBasicTimer>
#include <QAction>

class MediaPlayer;

class MediaVideoWidget : public Phonon::VideoWidget
{
    CS_OBJECT(MediaVideoWidget)
public:
    MediaVideoWidget(MediaPlayer *player, QWidget *parent = 0);
    void setStep(qint64 step) {m_step = step;}
    qint64 step() const {return m_step;}

public :
    CS_SLOT_1(Public, void setFullScreen(bool un_named_arg1))
    CS_SLOT_2(setFullScreen) 

public:
    CS_SIGNAL_1(Public, void fullScreenChanged(bool un_named_arg1))
    CS_SIGNAL_2(fullScreenChanged,un_named_arg1) 

protected:
    void mouseDoubleClickEvent(QMouseEvent *e);
    void keyPressEvent(QKeyEvent *e);
    bool event(QEvent *e);
    void timerEvent(QTimerEvent *e);
    qint64 m_step;

private:
    MediaPlayer *m_player;
    QBasicTimer m_timer;
    QAction m_action;
};

class MediaPlayer: public QWidget
{
    CS_OBJECT(MediaPlayer)
public:
    MediaPlayer(QWidget *parent = 0);
    ~MediaPlayer();
    void initVideoWindow();
    void loadFile(const QString & file);
    Phonon::MediaSource currentSource() const;
    Phonon::State state() const;
    qint64 currentTime() const;
    qint64 totalTime() const;
    QString currentFile() const;
    bool isSeekable() const;
    QStringList metaData(const QString & key) const;
    QStringList metaData(Phonon::MetaData key) const;
    const Phonon::MediaObject* mediaObject() const {return &m_MediaObject;}
    Phonon::VideoWidget* videoWidget() const {return m_videoWidget;}
    bool hasVideo() const;
    bool isPlaying() const;
    bool isPaused() const;
    bool isStopped() const;
    bool isError() const;
    bool isBuffering() const;
    bool isLoading() const;
    void clear();

public :
    CS_SLOT_1(Public, void play())
    CS_SLOT_2(play) 
    CS_SLOT_1(Public, void pause())
    CS_SLOT_2(pause) 
    CS_SLOT_1(Public, void stop())
    CS_SLOT_2(stop) 
    CS_SLOT_1(Public, void seek(qint64 time))
    CS_SLOT_2(seek) 

protected:

private:
    QWidget m_videoWindow;
    Phonon::SeekSlider* m_slider;
    Phonon::MediaObject m_MediaObject;
    Phonon::AudioOutput m_AudioOutput;
    MediaVideoWidget* m_videoWidget;
    Phonon::Path m_audioOutputPath;
};

#endif // MEDIAPLAYER_H
