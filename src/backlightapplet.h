#ifndef BACKLIGHTAPPLET_H
#define BACKLIGHTAPPLET_H

#include "applet.h"
#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QIcon>
#include <QPushButton>
#include <QSlider>
#include <X11/Xlib.h>
#include <X11/extensions/xf86vmode.h>

// undef the XLib None definition
#ifdef None
#undef None
#endif

class Ui_AppletBacklightSettings;

class BacklightApplet: public Applet
{
	CS_OBJECT(BacklightApplet)
public:
	BacklightApplet(PanelWindow* panelWindow);
	~BacklightApplet();

	bool init();
	QSize desiredSize();
	void clicked();
	void showContextMenu(const QPoint& point);

public :
	CS_SLOT_1(Public, void showConfigurationDialog())
	CS_SLOT_2(showConfigurationDialog) 
	CS_SLOT_1(Public, void valueChangedSlot(int value))
	CS_SLOT_2(valueChangedSlot) 
	CS_SLOT_1(Public, void buttonIconClicked())
	CS_SLOT_2(buttonIconClicked) 

protected:
	void layoutChanged();
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	bool xmlRead();
	void xmlReadIcon();
	void xmlWrite(XmlConfigWriter* writer);
	void setGamma(double brightness);
	bool getGamma();
	int doubleToInt(double value, double max);
	double intToDouble(int value, double max);

private:
	QGraphicsPixmapItem* m_pixmapItem;
	QIcon m_icon;
	int m_delta;
	QSlider* m_slider;
	Display* m_display;
	int m_screen;
	XF86VidModeGamma m_gamma;
	Ui_AppletBacklightSettings *m_settingsUi;
};

#endif //BACKLIGHTAPPLET_H
