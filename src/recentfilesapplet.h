#ifndef RECENTFILESAPPLET_H
#define RECENTFILESAPPLET_H

#include "applet.h"
#include <QDateTime>

class QGraphicsRectItem;
class QFile;
class QFileSystemWatcher;
class QString;
class TextGraphicsItem;
class QMutex;

struct BookmarkApplication
{
    QString name;
    QString exec;
    QDateTime modified;
    int count;
};

class RecentDocument
{
public:
    RecentDocument(QString href, QString mtype, QList<BookmarkApplication> bapps);
    ~RecentDocument();
    QString getBookmarkHref() {return bookmark_href;}
    QString getMimeType() {return mime_type;}
    QList<BookmarkApplication> getBookmarkApplications() {return bookmark_applications;}
    QString getPath() {return path;}
    void open();
private:
    QString bookmark_href;
    QString mime_type;
    QString path;
    QList<BookmarkApplication> bookmark_applications;
};

class RecentFilesApplet: public Applet
{
    CS_OBJECT(RecentFilesApplet)
public:
    RecentFilesApplet(PanelWindow* panelWindow);
    ~RecentFilesApplet();
    bool init();
    void xmlWrite(XmlConfigWriter* writer);
    void showContextMenu(const QPoint& point);
    QSize desiredSize();

public :
    CS_SLOT_1(Public, void showConfigurationDialog())
    CS_SLOT_2(showConfigurationDialog)

protected:
    void layoutChanged();
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    bool xmlRead();
    QList<RecentDocument> getRecentDocuments();

private:
    QFile* m_file;
    QString m_namefile;
    QFileSystemWatcher* m_watcher;
    TextGraphicsItem* m_textItem;
    int m_numItemRecentFiles;
};

#endif // RECENTFILESAPPLET_H
