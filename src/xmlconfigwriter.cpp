#include <QSettings>
#include <QFileInfo>
#include <QDir>
#include "xmlconfigwriter.h"

XmlConfigWriter::XmlConfigWriter()
{
    XmlConfigWriter("config.xml");
}

XmlConfigWriter::XmlConfigWriter(const QString& namefile)
{
    m_namefile = namefile;

    MyDBG(m_namefile);

    QSettings settings;
    QFileInfo fileInfo(settings.fileName());
    QDir dir(fileInfo.absoluteDir());
    QString xml_path_file(dir.absolutePath() + QDir::separator() + m_namefile);

    MyDBG(xml_path_file);

    m_xmlFile.setFileName(xml_path_file);
}

XmlConfigWriter::~XmlConfigWriter()
{
    xmlClose();
}

bool XmlConfigWriter::xmlOpen()
{
    bool ret = m_xmlFile.open(QIODevice::WriteOnly | QIODevice::Text);
    if (ret) {

        MyDBG("Open" << m_xmlFile.fileName() << "OK");

        setDevice(&m_xmlFile);
        setAutoFormatting(true);
        setAutoFormattingIndent(2);
    }
    return ret;
}

void XmlConfigWriter::xmlClose()
{
    m_xmlFile.close();
}
