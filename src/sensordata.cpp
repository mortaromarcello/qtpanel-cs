#include "sensordata.h"
#include <unistd.h>
#include <iostream>
#include <sstream>

//#define TEST

using namespace std;

SensorsSubFeature::SensorsSubFeature(const sensors_chip_name *cn, const sensors_feature *feat, const sensors_subfeature *subf)
    : m_chip_name(cn),
    m_feature(feat),
    m_subfeature(subf)
{}

double SensorsSubFeature::getValue()
{
    double value;
    int rc = sensors_get_value(m_chip_name, getNumber(), &value);
    if (rc < 0)
        value = rc;
    return value;
}

SensorsFeature::SensorsFeature(const sensors_chip_name *cn, const sensors_feature *feat)
    : m_chip_name(cn),
    m_feature(feat)
{
    int s = 0;
    const sensors_subfeature *subf;
    while ((subf = sensors_get_all_subfeatures(cn, feat, &s)) != 0) 
        m_sensors_subfeature.push_back(new SensorsSubFeature(cn, m_feature, subf));
}

string SensorsFeature::getLabel()
{
    return string(sensors_get_label(m_chip_name, m_feature));
}

SensorsChipName::SensorsChipName(const sensors_chip_name *cn):m_chip_name(cn)
{
    sensors_feature const * feat;
    int f = 0;
    while ((feat = sensors_get_features(m_chip_name, &f)) != 0) 
        m_sensors_feature.push_back(new SensorsFeature(m_chip_name, feat));
}

SensorsFeature *SensorsChipName::getSensorsFeature(const string& label)
{
    for (SensorsFeature *feat: getListSensorsFeature())
        if (feat->getLabel() == label)
            return feat;
    return nullptr;
}

SensorsData::SensorsData()
{
    sensors_init(NULL);
    const sensors_chip_name * cn;
    int c = 0;
    while ((cn = sensors_get_detected_chips(0, &c)) != 0) 
        m_sensors_chip_name.push_back(new SensorsChipName(cn));
}

SensorsChipName * SensorsData::getSensorsChipName(const string& prefix)
{
    for (SensorsChipName *scn: getListSensorsChipName())
        if (scn->getPrefix() == prefix)
            return scn;
    return nullptr;
}

#ifdef TEST
int main(int argc, char** argv)
{
    SensorsData sensor;
    for (SensorsChipName scn: sensor.getListSensorsChipName())
        for (SensorsFeature sf: scn.getListSensorsFeature())
            for (SensorsSubFeature ssf: sf.getListSensorsSubFeature())
                if (ssf.getType() == SENSORS_SUBFEATURE_TEMP_INPUT)
                    cout << sf.getLabel() << "=" << ssf.getValue() << endl;
}

#endif
