#ifndef COMPMGR_H
#define COMPMGR_H

#include <QThread>

class CompMgr: public QThread
{
	CS_OBJECT(CompMgr)
public:
    CompMgr():m_stop(false) {}
    ~CompMgr() {}
public :
    CS_SLOT_1(Public, void stop())
    CS_SLOT_2(stop) 
private:
	void run();
	bool m_stop;
};

#endif // COMPMGR_H
