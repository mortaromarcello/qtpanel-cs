#include "qtpanelconfig.h"
#include "panelapplication.h"
#include <QTranslator>
#include <signal.h>
#include <stdio.h>
#include <QLocale>
#include <QProcessEnvironment>
#include <QDir>
#include "xdg.h"

void myMessageOutput(QtMsgType type, const char *msg)
{
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s\n", msg);
        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s\n", msg);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s\n", msg);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s\n", msg);
        abort();
    }
}

int main(int argc, char** argv)
{
    qInstallMsgHandler(myMessageOutput);
    Q_INIT_RESOURCE(qtpanelcs);
    QString defaultHomeCsDir = QDir::homePath() + "/copperspice";
    QString csLibrary = defaultHomeCsDir + "/lib";
    XdgDirectory xdg;
    QProcessEnvironment environment = xdg.getEnvironment();
    MyDBG(environment.toStringList());
    MyDBG(xdg.getHome());
    MyDBG(xdg.getXdgDataHome());
    MyDBG(xdg.getXdgConfigHome());
    MyDBG(xdg.getXdgCacheHome());
    MyDBG(xdg.getXdgDataDirs());
    MyDBG(xdg.getXdgConfigDirs());
    MyDBG(xdg.getXdgAutostartHome());
    MyDBG(xdg.getXdgAutostartDirs());
    MyDBG(xdg.getXdgTrashHome());
    QStringList list = QStringList() << "/usr/lib" << "/usr/local/lib";
    for (QString str: list)
    {
        if (QFile::exists(str + "/libCsCore1.4.so"))
        {
            csLibrary = str;
            break;
        }
    }

    if (QFile::exists(csLibrary + "/libCsCore1.4.so") == false)
    {
        MyDBG("Error!");
        return -1;
    }
    QApplication::addLibraryPath(csLibrary);
    MyDBG(QCoreApplication::libraryPaths());
    
    PanelApplication app(argc, argv);
    QTranslator translator;
    if (translator.load(QLocale::system(), app.getApplicationName(), "_", ":/translations"))
    {
        MyDBG(QObject::tr("File translation loaded."));
    }
    else
        MyDBG(QObject::tr("File traslation not loaded"));
    MyDBG(QLocale::system().name());
    MyDBG(app.getApplicationName());
    app.installTranslator(&translator);
    app.init();
    return app.exec();
}
