#ifndef APPLETS_H
#define APPLETS_H

#include <QStringList>

// lista degli applets

const QStringList listApplets =
    QStringList()   << "ApplicationsMenuApplet"
                    << "BacklightApplet"
                    << "BatteryApplet"
                    << "ClockApplet"
                    << "DemoApplet"
                    << "DockApplet"
                    << "DockLauncherApplet"
                    << "MediaApplet2"
                    << "MemoryApplet"
                    << "PagerApplet"
                    << "RecentFilesApplet"
                    << "SessionApplet"
                    << "SpacerApplet"
                    << "SensorApplet"
                    << "TrayApplet"
                    << "VolumeApplet";

#endif // APPLETS_H
