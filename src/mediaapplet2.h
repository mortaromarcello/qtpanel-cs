#ifndef MEDIAAPPLET2_H
#define MEDIAAPPLET2_H

#include "applet.h"
#include "mediaplayer.h"
#include <QIcon>
#include <QFileInfo>
#include <QGraphicsPixmapItem>
#include <phonon/mediaobject.h>

//class MediaPlayer;
class QAction;

class MediaButton: public QObject, public QGraphicsPixmapItem
{
    CS_OBJECT(MediaButton)
public:
    MediaButton(const QString& icon, MediaPlayer* mediaplayer, QGraphicsItem * parent = 0);
    ~MediaButton() {}
    const QIcon& icon() const {return m_icon;}
    bool useInternalIcon() {return m_useInternalIcon;}
    void setUseInternalIcon(bool value) {m_useInternalIcon = value;}
    void setIcon(const QString& icon);
    void setIconPixmap(QIcon::Mode mode = QIcon::Normal, int extent = 24);
    void play() { m_mediaPlayer->play(); }
    void pause() { m_mediaPlayer->pause(); }
    void stop() { m_mediaPlayer->stop(); }
    bool isPlaying() { return m_mediaPlayer->isPlaying(); }
    bool isPaused() { return m_mediaPlayer->isPaused(); }
    bool isStopped() { return m_mediaPlayer->isStopped(); }

private:
    bool m_useInternalIcon;
    QIcon m_icon;
    MediaPlayer* m_mediaPlayer;
};

class MediaButtonPrev: public MediaButton
{
    CS_OBJECT(MediaButtonPrev)
public:
    MediaButtonPrev(MediaPlayer* mediaplayer, QGraphicsItem* parent = 0):MediaButton("media-skip-backward", mediaplayer, parent) {}
};

class MediaButtonPlayPause: public MediaButton
{
    CS_OBJECT(MediaButtonPlayPause)
public:
    MediaButtonPlayPause(MediaPlayer* mediaplayer, QGraphicsItem * parent = 0):MediaButton("media-playback-start.png", mediaplayer, parent) {}
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    CS_SLOT_1(Public, void enableButton(Phonon::State newstate, Phonon::State oldstate)
    {
        switch (newstate) {
            case Phonon::StoppedState:
                setIcon("media-playback-start.png");
                setIconPixmap();
                break;
            case Phonon::PlayingState:
                setIcon("media-playback-pause.png");
                setIconPixmap();
                break;
            case Phonon::PausedState:
                setIcon("media-playback-start.png");
                setIconPixmap();
                break;
            default:
                setIcon("media-playback-start.png");
                setIconPixmap(QIcon::Disabled);
                break;
        }
    })
    CS_SLOT_2(enableButton)
};

class MediaButtonStop: public MediaButton
{
    CS_OBJECT(MediaButtonStop)
public:
    MediaButtonStop(MediaPlayer* mediaplayer, QGraphicsItem * parent = 0):MediaButton("media-playback-stop.png", mediaplayer, parent) {}
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    CS_SLOT_1(Public, void enableButton(Phonon::State newstate, Phonon::State oldstate)
    {
        switch (newstate) {
            case Phonon::StoppedState:
                setIconPixmap(QIcon::Disabled);
                break;
            case Phonon::PlayingState:
                setIconPixmap();
                break;
            case Phonon::PausedState:
                setIconPixmap();
                break;
            default:
                setIconPixmap(QIcon::Disabled);
                break;
        }
    })
    CS_SLOT_2(enableButton)
};

class MediaButtonNext: public MediaButton
{
    CS_OBJECT(MediaButtonNext)
public:
    MediaButtonNext(MediaPlayer* mediaplayer, QGraphicsItem* parent = 0):MediaButton("media-skip-forward", mediaplayer, parent) {}
};

class MediaApplet2: public Applet
{
    CS_OBJECT(MediaApplet2)
public:
    MediaApplet2(PanelWindow* panelWindow);
    ~MediaApplet2();
    bool init();
    void showContextMenu(const QPoint& point);
    QSize desiredSize();

public :
    CS_SLOT_1(Public, void showConfigurationDialog())
    CS_SLOT_2(showConfigurationDialog) 
    CS_SLOT_1(Public, void openFile())
    CS_SLOT_2(openFile)
    CS_SLOT_1(Public, void stateChanged(Phonon::State newstate, Phonon::State oldstate))
    CS_SLOT_2(stateChanged)
    CS_SLOT_1(Public, void hasVideoChanged(bool changed))
    CS_SLOT_2(hasVideoChanged)
 

protected:
    void createFileMenu();
    void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);

private:
    QMenu* m_fileMenu;
    QAction* m_openFileAction;
    QGraphicsPixmapItem* m_open;
    MediaButtonPrev* m_prev;
    MediaButtonPlayPause* m_playPause;
    MediaButtonStop* m_stop;
    MediaButtonNext* m_next;
    MediaPlayer* m_player;
    QString m_currentPath;
};

#endif // MEDIAAPPLET_H
