#ifndef SPACERAPPLET_H
#define SPACERAPPLET_H

#include "applet.h"

class SpacerApplet: public Applet
{
	CS_OBJECT(SpacerApplet)
public:
	SpacerApplet(PanelWindow* panelWindow);
	~SpacerApplet();

	QSize desiredSize();
};

#endif
