#ifndef MEDIAAPPLET_H
#define MEDIAAPPLET_H

#include "applet.h"
#include <QIcon>

class MediaPlayer;
class QGraphicsPixmapItem;
class QAction;

class MediaApplet: public Applet
{
	CS_OBJECT(MediaApplet)
public:
	MediaApplet(PanelWindow* panelWindow);
	~MediaApplet();
	bool init();
	void xmlWrite(XmlConfigWriter* writer);
	void showContextMenu(const QPoint& point);
	QSize desiredSize();
	void prev();
	void playPause();
	void stop();
	void next();

public :
	CS_SLOT_1(Public, void showConfigurationDialog())
	CS_SLOT_2(showConfigurationDialog) 
	CS_SLOT_1(Public, void openFile())
	CS_SLOT_2(openFile) 
	CS_SLOT_1(Public, void addFile())
	CS_SLOT_2(addFile) 
	CS_SLOT_1(Public, void hasVideoChanged(bool changed))
	CS_SLOT_2(hasVideoChanged) 
	CS_SLOT_1(Public, void finished())
	CS_SLOT_2(finished) 
	CS_SLOT_1(Public, void stateChanged())
	CS_SLOT_2(stateChanged) 
	CS_SLOT_1(Public, void metaDataChanged())
	CS_SLOT_2(metaDataChanged) 
	CS_SLOT_1(Public, void playMediaFromMenu(QAction * action))
	CS_SLOT_2(playMediaFromMenu) 
	CS_SLOT_1(Public, void memorizeToggled(bool checked))
	CS_SLOT_2(memorizeToggled) 
	CS_SLOT_1(Public, void reloadToggled(bool checked))
	CS_SLOT_2(reloadToggled) 
	CS_SLOT_1(Public, void loopToggled(bool checked){m_loopMedia=checked;})
	CS_SLOT_2(loopToggled) 
	CS_SLOT_1(Public, void clearMenuFiles())
	CS_SLOT_2(clearMenuFiles) 

protected:
	bool nextMedia(bool remove = false);
	bool prevMedia(bool remove = false);
	QString getNextMedia();
	QString getPrevMedia();
	void createFileMenu();
	void setCurrentMetaData();
	void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
	void mousePressEvent(QGraphicsSceneMouseEvent* event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
	bool xmlRead();
	void xmlReadMemorizedPos();
	void xmlReadCurrentMedia();
	void xmlReadMediaFile();
	void xmlReadReloadLastSession();
	void xmlReadLoopMedia();

private:
	const QString getCurrentMetaData() const;
	void setIcon(QGraphicsPixmapItem* item, const QString &icon, QIcon::Mode mode = QIcon::Normal, int extent = 24, const QString &ext = "");
	void setIcons();
	void clearCurrentMetaData();
	QMenu* m_fileMenu;
	QAction* m_openFileAction;
	QAction* m_addFileAction;
	QAction* m_clearMenuFilesAction;
	QGraphicsPixmapItem* m_prev;
	QGraphicsPixmapItem* m_playPause;
	QGraphicsPixmapItem* m_stop;
	QGraphicsPixmapItem* m_next;
	MediaPlayer* m_player;
	QString m_currentPath;
	QStringList m_currentMetaArtist;
	QStringList m_currentMetaAlbum;
	QStringList m_currentMetaTitle;
	QStringList m_currentMetaDate;
	QStringList m_currentMetaGenre;
	QStringList m_currentMetaTrack;
	QStringList m_currentMetaComment;
	QMap<QString, qint64> m_listMediaFile;
	QMap<QString, QAction*> m_listMediaAction;
	bool m_useInternalIcon;
	bool m_memorizePositionMediaFiles;
	bool m_reloadLastSession;
	qint64 m_lastCurrentMediaPosition;
	bool m_loopMedia;
};

#endif // MEDIAAPPLET_H
