#include "sensorapplet.h"
#include "textgraphicsitem.h"
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QToolTip>
#include "panelwindow.h"
#include "ui_appletsensorsettings.h"
#include "panelapplication.h"

SensorApplet::SensorApplet(PanelWindow* panelWindow)
    : Applet(panelWindow), 
    m_chip(m_data.getListSensorsChipName().front()),
    m_label("Core 0"),
    m_color(Qt::blue),
    m_textItem(new TextGraphicsItem(this)),
    m_settingsUi(new Ui::AppletSensorSettings())
{
    if (!xmlRead()) {
        qWarning("Don't read configuration applet file.");
    }
}

SensorApplet::~SensorApplet()
{
    delete m_textItem;
    delete m_settingsUi;
    m_timer.stop();
}

bool SensorApplet::init()
{
    QString values;
    SensorsFeature *feat = m_chip->getSensorsFeature(m_label.toUtf8().constData());
    if (feat)
    {
        SensorsSubFeature *subf= feat->getListSensorsSubFeature().front();
        if (subf->getType() == SENSORS_SUBFEATURE_TEMP_INPUT)
        values = QString().setNum(subf->getValue()) + "C";
        m_textItem->setText(values);
    }
    m_textItem->setText(values);
    m_textItem->setColor(m_color);
    m_textItem->setFont(PanelApplication::instance()->panelFont());
    connect(&m_timer, SIGNAL(timeout()), this, SLOT(updateInfo()));
    m_timer.start(3000);
    setInteractive(true);
    return true;
}

void SensorApplet::xmlWrite(XmlConfigWriter* writer)
{
    writer->writeStartElement("config");
    writer->writeTextElement("chipset", QString::fromUtf8(m_chip->getPrefix().c_str()));
    writer->writeTextElement("label", m_label);
    writer->writeTextElement("color", m_color.name());
    writer->writeEndElement();
}

void SensorApplet::showConfigurationDialog()
{
    QDialog dialog;
    QList <QString> chipList;
    QList <QString> labelList;
    QStringList colors = QColor::colorNames();
    QList <QColor> colorList;
    for (QString color: colors)
        colorList << QColor(color);
    for (SensorsChipName *chip: m_data.getListSensorsChipName())
        chipList << QString::fromUtf8(chip->getPrefix().c_str());
    m_settingsUi->setupUi(&dialog);
    connect(m_settingsUi->chipset, SIGNAL(currentIndexChanged(const QString &)), SLOT(readLabels(const QString &)));
    m_settingsUi->chipset->addItems(chipList);
    m_settingsUi->chipset->setCurrentIndex(chipList.indexOf(QString(m_chip->getPrefix().c_str())));
    m_settingsUi->color->addItems(colors);
    m_settingsUi->color->setCurrentIndex(colorList.indexOf(m_color));
    if(dialog.exec() == QDialog::Accepted) {
        m_chip = m_data.getSensorsChipName(m_settingsUi->chipset->currentText().toUtf8().constData());
        m_label = m_settingsUi->label->currentText();
        m_color = QColor(m_settingsUi->color->currentText());
        m_textItem->setColor(m_color);
    }
}

void SensorApplet::updateInfo()
{
    MyDBG("updateInfo SensorApplet");
    QString values;
    SensorsFeature *feat = m_chip->getSensorsFeature(m_label.toUtf8().constData());
    MyDBG(feat);
    MyDBG(m_label);
    if (feat) 
    {
        SensorsSubFeature *subf= feat->getListSensorsSubFeature().front();
        if (subf->getType() == SENSORS_SUBFEATURE_TEMP_INPUT)
        values = QString().setNum(subf->getValue()) + "C";
        m_textItem->setText(values);
    }
    values = "";
    for(SensorsFeature *feat: m_chip->getListSensorsFeature())
    {
        for (SensorsSubFeature *subf: feat->getListSensorsSubFeature())
        {
            if (subf->getType() == SENSORS_SUBFEATURE_TEMP_INPUT)
                values = values + QString::fromUtf8(feat->getLabel().c_str()) + "=" + QString().setNum(subf->getValue()) + "\n";
        }
    }
    m_textToolTip = values;
    if (QToolTip::isVisible() && isUnderMouse())
        QToolTip::showText(localToScreen(QPoint(0, (m_panelWindow->verticalAnchor() == PanelWindow::Bottom) ? 0 : 16)), m_textToolTip);
    MyDBG(m_textToolTip);
}

void SensorApplet::readLabels(const QString &index)
{
    QList <QString> labelList;
    MyDBG("readLabels");
    SensorsChipName *chip = m_data.getSensorsChipName(index.toUtf8().constData());
    for (SensorsFeature *feat: chip->getListSensorsFeature())
        labelList << QString::fromUtf8(feat->getLabel().c_str());
    m_settingsUi->label->clear();
    m_settingsUi->label->addItems(labelList);
    if ((! m_label.isEmpty()) && labelList.contains(m_label))
        m_settingsUi->label->setCurrentIndex(labelList.indexOf(m_label));
}

void SensorApplet::showContextMenu(const QPoint& point)
{
    QMenu menu;
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure..."), this, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-desktop"), tr("Configure Panel"), PanelApplication::instance(), SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure applets"), m_panelWindow, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("remove"), tr("Remove applet"), this, SLOT(removeApplet()));
    menu.exec(point);
}

void SensorApplet::layoutChanged()
{
    m_textItem->setPos((m_size.width() - m_textItem->boundingRect().size().width())/2.0, m_panelWindow->textBaseLine());
    update();

}

QSize SensorApplet::desiredSize()
{
    return QSize(32, 32);
}

void SensorApplet::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if (event->buttons() == Qt::RightButton)
        showContextMenu(localToScreen(QPoint(0, 0)));
}

void SensorApplet::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    //if(isUnderMouse())
    //    showContextMenu(localToScreen(QPoint(0, 0)));
}

bool SensorApplet::xmlRead()
{
    if (!m_xmlConfigReader.xmlOpen()) {
        MyDBG("Error opening file.");
#ifndef __DEBUG__
        qDebug("Error opening file.");
#endif
        return false;
    }
    while (!m_xmlConfigReader.atEnd()) {
        if (m_xmlConfigReader.hasError()) {
            m_xmlConfigReader.xmlErrorString();
            m_xmlConfigReader.xmlClose();
            return false;
        }
        while (m_xmlConfigReader.readNextStartElement())
            if (m_xmlConfigReader.name() == "applet")
            while (m_xmlConfigReader.readNextStartElement())
                if (m_xmlConfigReader.name() == "type")
                    if (m_xmlConfigReader.readElementText() == getNameApplet())
                        while (m_xmlConfigReader.readNextStartElement())
                            if (m_xmlConfigReader.name() == "config")
                                while (m_xmlConfigReader.readNextStartElement())
                                    if (m_xmlConfigReader.name() == "chipset")
                                        xmlReadChipset();
                                    else if (m_xmlConfigReader.name() == "label")
                                        xmlReadLabel();
                                    else if (m_xmlConfigReader.name() == "color")
                                        xmlReadColor();
    }
    m_xmlConfigReader.xmlClose();
    return true;
}

void SensorApplet::xmlReadChipset()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "chipset");

    MyDBG(m_xmlConfigReader.name());
    m_chip = m_data.getSensorsChipName(m_xmlConfigReader.readElementText().toUtf8().constData());
}

void SensorApplet::xmlReadLabel()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "label");
    MyDBG(m_xmlConfigReader.name());
    m_label = m_xmlConfigReader.readElementText();
}

void SensorApplet::xmlReadColor()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "color");
    MyDBG(m_xmlConfigReader.name());
    m_color.setNamedColor(m_xmlConfigReader.readElementText());
}
