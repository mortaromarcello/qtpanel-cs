#ifndef TRAYAPPLET_H
#define TRAYAPPLET_H

#include <QVector>
#include <QSize>
#include "applet.h"

class TrayApplet;

class TrayItem: public QObject, public QGraphicsItem
{
	CS_OBJECT(TrayItem)
	CS_INTERFACES(QGraphicsItem)public:
	TrayItem(TrayApplet* trayApplet, unsigned long window);
	~TrayItem();

	void setPosition(const QPoint& position);
	void setSize(const QSize& size);

	QRectF boundingRect() const;
	void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);

	unsigned long window() const
	{
		return m_window;
	}

private:
	QSize m_size;
	TrayApplet* m_trayApplet;
	unsigned long m_window;
};

class TrayApplet: public Applet
{
	CS_OBJECT(TrayApplet)
public:
	TrayApplet(PanelWindow* panelWindow);
	~TrayApplet();

	bool init();

	QSize desiredSize();

	void registerTrayItem(TrayItem* trayItem);
	void unregisterTrayItem(TrayItem* trayItem);

	int iconSize() const { return m_iconSize; }

protected:
	void layoutChanged();

private :
	CS_SLOT_1(Private, void clientMessageReceived(unsigned long window,unsigned long atom,void * data))
	CS_SLOT_2(clientMessageReceived) 
	CS_SLOT_1(Private, void windowClosed(unsigned long window))
	CS_SLOT_2(windowClosed) 
	CS_SLOT_1(Private, void windowReconfigured(unsigned long window,int x,int y,int width,int height))
	CS_SLOT_2(windowReconfigured) 
	CS_SLOT_1(Private, void windowDamaged(unsigned long window))
	CS_SLOT_2(windowDamaged) 

private:
	void updateLayout();

	bool m_initialized;
	QVector<TrayItem*> m_trayItems;
	int m_iconSize;
	int m_spacing;
};

#endif
