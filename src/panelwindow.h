#ifndef PANELWINDOW_H
#define PANELWINDOW_H

#include <QVector>
#include <QWidget>
#include <QGraphicsItem>
#include <QSettings>
#include "xmlconfigreader.h"
#include "xmlconfigwriter.h"

class QFont;
class QGraphicsScene;
class QGraphicsView;
class Applet;
class PanelWindow;

class PanelWindowGraphicsItem: public QGraphicsItem
{
public:
    PanelWindowGraphicsItem(PanelWindow* panelWindow);
    ~PanelWindowGraphicsItem();

    QRectF boundingRect() const;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
private:
    PanelWindow* m_panelWindow;
};

class PanelWindow: public QWidget
{
    CS_OBJECT(PanelWindow)
public:
    PanelWindow();
    ~PanelWindow();
    static PanelWindow* instance() {return m_instance;}
    
    enum Anchor
    {
        Top,
        Bottom
    };

    enum LayoutPolicy
    {
        Normal,
        AutoSize,
        FillSpace
    };

    bool init();
    int screen() const {return m_screen;}
    void setScreen(int screen);
    QSettings* copperSpiceConf() const {return m_copperSpiceConf;}
    Anchor verticalAnchor() const {return m_verticalAnchor;}
    void setVerticalAnchor(Anchor verticalAnchor);
    LayoutPolicy layoutPolicy() const {return m_layoutPolicy;}
    void setLayoutPolicy(LayoutPolicy layoutPolicy);
    void updatePosition();
    const QFont& font() const;
    int textBaseLine();
    PanelWindowGraphicsItem* panelItem() {return m_panelItem;}
    void resizeEvent(QResizeEvent* event);
    void updateLayout();
    void showPanelContextMenu(const QPoint& point);
    bool xmlRead();
    void xmlReadType();
    void xmlWrite();
    bool isConfigAppletsChanged();
    void removeApplet(Applet* applet);

public :
    CS_SLOT_1(Public, void showConfigurationDialog())
    CS_SLOT_2(showConfigurationDialog) 

protected:
    XmlConfigWriter m_xmlConfigWriter;
private:
    int m_screen;
    Anchor m_verticalAnchor;
    LayoutPolicy m_layoutPolicy;

    QGraphicsScene* m_scene;
    QGraphicsView* m_view;
    PanelWindowGraphicsItem* m_panelItem;
    QVector<Applet*> m_applets;
    XmlConfigReader m_xmlConfigReader;
    QSettings* m_copperSpiceConf;
private:
    static PanelWindow* m_instance;
};

#endif
