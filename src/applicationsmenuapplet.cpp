#include "applicationsmenuapplet.h"

#include <QMenu>
#include <QStyle>
#include <QPixmap>
#include <QGraphicsScene>
#include <QDialog>
#include <QFileDialog>
#include <QGraphicsSceneMouseEvent>
#include <QTimer>
#include <QMessageBox>
#include <QProcess>
#include "textgraphicsitem.h"
#include "panelwindow.h"
#include "desktopapplications.h"
#include "dpisupport.h"
#include "qtpanelconfig.h"
#include "mydebug.h"
#include "ui_appletapplicationsmenusettings.h"
#include "panelapplication.h"
#include "runprocess.h"

int ApplicationsMenuStyle::pixelMetric(PixelMetric metric, const QStyleOption* option, const QWidget* widget) const
{
    if(metric == QStyle::PM_SmallIconSize)
        return adjustHardcodedPixelSize(24);
    else
        return QCleanlooksStyle::pixelMetric(metric, option, widget);
}

SubMenu::SubMenu(QMenu* parent, const QString& title, const QString& category, const QString& icon)
{
    m_menu = new QMenu(parent); // Will be deleted automatically.
    //
    m_menu->setAttribute(Qt::WA_TranslucentBackground, true);
    //
    m_menu->setStyle(parent->style());
    m_menu->setFont(parent->font());
    m_menu->setTitle(title);
    m_menu->setIcon(QIcon::fromTheme(icon));
    m_menu->menuAction()->setIconVisibleInMenu(true);
    m_category = category;
}

static const char* menuStyleSheet =
"QMenu { border: %1px solid %2; border-top-left-radius: %3px; border-top-right-radius: %4px; border-bottom-left-radius: %5px; border-bottom-right-radius: %6px; background-color: %7; margin-right: 2px }\n"
"QMenu::item { height: %8px; background-color: transparent; color: %9; padding-left: %10px; padding-right: %11px; padding-top: %12px; padding-bottom: %13px; margin-right: 2px }\n"
"QMenu::item::selected { background-color: %14; border: %1px solid white; padding-left: %10px; padding-right: %11px; padding-top: %12px; padding-bottom: %13px; border-top-left-radius: %3px; border-top-right-radius: %4px; border-bottom-left-radius: %5px; border-bottom-right-radius: %6px; color: %15 }\n"
"QMenu::icon { left: %16px; }\n"
"QMenu::icon::checked { /* appearance of a 'checked' icon */ background: gray; border: 1px inset gray; position: absolute; top: 1px; right: 1px; bottom: 1px; left: 1px; }\n"
"QMenu::separator { height: 2px; background: lightblue; margin-left: 10px; margin-right: 5px; }\n"
"QMenu::indicator { width: 13px; height: 13px; }\n"
"QMenu::indicator:non-exclusive:unchecked { image: url(:/images/checkbox_unchecked.png); }\n"
"QMenu::indicator:non-exclusive:unchecked:selected { image: url(:/images/checkbox_unchecked_hover.png); }\n"
"QMenu::indicator:non-exclusive:checked { image: url(:/images/checkbox_checked.png); }\n"
"QMenu::indicator:non-exclusive:checked:selected { image: url(:/images/checkbox_checked_hover.png); }\n"
"QMenu::indicator:exclusive:unchecked { image: url(:/images/radiobutton_unchecked.png); }\n"
"QMenu::indicator:exclusive:unchecked:selected { image: url(:/images/radiobutton_unchecked_hover.png); }\n"
"QMenu::indicator:exclusive:checked { image: url(:/images/radiobutton_checked.png); }\n"
"QMenu::indicator:exclusive:checked:selected { image: url(:/images/radiobutton_checked_hover.png); }\n";

ApplicationsMenuApplet::ApplicationsMenuApplet(PanelWindow* panelWindow)
    : Applet(panelWindow),
    m_menuOpened(false),
    m_opacity(0.8),
    m_backgroundMenuColor(QColor("gray")),
    m_textMenuColor(QColor("white")),
    m_selectedMenuColor(QColor("darkGray")),
    m_backgroundSelectedMenuColor(QColor("orangered")),
    m_menuBorderColor(QColor("greenyellow")),
    m_menuBorderWidth(2),
    m_menuBorderRadiusTopLeft(0),
    m_menuBorderRadiusTopRight(0),
    m_menuBorderRadiusBottomLeft(0),
    m_menuBorderRadiusBottomRight(8)
{
    m_menuStyleSheet = QString(menuStyleSheet);
    if (!xmlRead()) {
        qWarning("Don't read configuration applet file.");
    }
    m_menu = new QMenu();
    m_menu->setAttribute(Qt::WA_TranslucentBackground, true);
    m_menu->setStyle(&m_style);
    m_menu->setFont(m_panelWindow->font());
    m_menu->setStyleSheet(m_menuStyleSheet
        .arg(adjustHardcodedPixelSize(m_menuBorderWidth))               // 1
        .arg(m_menuBorderColor.name())                                  // 2
        .arg(adjustHardcodedPixelSize(m_menuBorderRadiusTopLeft))       // 3
        .arg(adjustHardcodedPixelSize(m_menuBorderRadiusTopRight))      // 4
        .arg(adjustHardcodedPixelSize(m_menuBorderRadiusBottomLeft))    // 5
        .arg(adjustHardcodedPixelSize(m_menuBorderRadiusBottomRight))   // 6
        .arg(m_backgroundMenuColor.name())                              // 7
        .arg(adjustHardcodedPixelSize(24))                              // 8
        .arg(m_textMenuColor.name())                                    // 9
        .arg(adjustHardcodedPixelSize(32))                              // 10
        .arg(adjustHardcodedPixelSize(16))                              // 11
        .arg(adjustHardcodedPixelSize(2))                               // 12
        .arg(adjustHardcodedPixelSize(2))                               // 13
        .arg(m_backgroundSelectedMenuColor.name())                              // 14
        .arg(m_selectedMenuColor.name())                        // 15
        .arg(adjustHardcodedPixelSize(2))                               // 16
    );
    m_subMenus.append(SubMenu(m_menu, tr("Accessories"), "Utility", "applications-accessories"));
    m_subMenus.append(SubMenu(m_menu, tr("Development"), "Development", "applications-development"));
    m_subMenus.append(SubMenu(m_menu, tr("Education"), "Education", "applications-science"));
    m_subMenus.append(SubMenu(m_menu, tr("Office"), "Office", "applications-office"));
    m_subMenus.append(SubMenu(m_menu, tr("Graphics"), "Graphics", "applications-graphics"));
    m_subMenus.append(SubMenu(m_menu, tr("Multimedia"), "AudioVideo", "applications-multimedia"));
    m_subMenus.append(SubMenu(m_menu, tr("Games"), "Game", "applications-games"));
    m_subMenus.append(SubMenu(m_menu, tr("Network"), "Network", "applications-internet"));
    m_subMenus.append(SubMenu(m_menu, tr("System"), "System", "preferences-system"));
    m_subMenus.append(SubMenu(m_menu, tr("Settings"), "Settings", "preferences-desktop"));
    m_subMenus.append(SubMenu(m_menu, tr("Other"), "Other", "applications-other"));
//
    setOpacity(m_opacity);
    m_pixmapItem = new QGraphicsPixmapItem(this);
    if (m_icon.name().isEmpty())
        m_icon = QIcon(":/images/linux.png");
    m_pixmapItem->setPixmap(m_icon.pixmap(adjustHardcodedPixelSize(24), adjustHardcodedPixelSize(24)));
    m_pixmapItem->setOffset(4, 4);
//
    m_textItem = new TextGraphicsItem(this);
    m_textItem->setColor(Qt::white);
    m_textItem->setFont(m_panelWindow->font());
    m_textItem->setText(tr("Applications"));
    m_settingsUi = new Ui::AppletApplicationsMenuSettings;
}

ApplicationsMenuApplet::~ApplicationsMenuApplet()
{
    for(QAction* action: m_actions)
    {
        delete action;
    }

    delete m_textItem;
    //
    delete m_pixmapItem;
    delete m_settingsUi;
    //
    delete m_menu;
}

bool ApplicationsMenuApplet::init()
{
    setInteractive(true);

    connect(DesktopApplications::instance(), SIGNAL(applicationUpdated(const DesktopApplication&)), this, SLOT(applicationUpdated(const DesktopApplication&)));
    connect(DesktopApplications::instance(), SIGNAL(applicationRemoved(const QString&)), this, SLOT(applicationRemoved(const QString&)));
    QList<DesktopApplication> apps = DesktopApplications::instance()->applications();
    for(const DesktopApplication& app: apps) {
        applicationUpdated(app);
        MyDBG("Path File Entry =" << app.path() << "\nLast Updated =" << app.lastUpdated() << "\nIs No Display =" << app.isNoDisplay() << "\n---XDG Values---\nName =" << app.name() << "\nExec =" << app.exec() << "\nIcon =" << app.iconName() << "\nCategories =" << app.categories().join(";") << "\nType =" << app.type() << "\nGenericName =" << app.genericName() << "\nComment =" << app.comment() << "\nTerminal =" << (app.terminal() ? "True":"False") << "\nMimeType =" << app.mimeType() << "\nPath =" << app.workPath());
    }

    return true;
}

QSize ApplicationsMenuApplet::desiredSize()
{
    return QSize(m_textItem->boundingRect().size().width() + 8 + m_pixmapItem->boundingRect().size().width(), m_textItem->boundingRect().size().height());
}

void ApplicationsMenuApplet::clicked()
{
    m_menuOpened = true;
    animateHighlight();

    m_menu->move(localToScreen(QPoint(0, m_size.height())));
    m_menu->exec();

    m_menuOpened = false;
    animateHighlight();
}

void ApplicationsMenuApplet::xmlWrite(XmlConfigWriter* writer)
{
    writer->writeStartElement("config");
    writer->writeTextElement("menu-icon", m_icon.name());
    writer->writeTextElement("opacity", QString("%1").arg(m_opacity));
    writer->writeTextElement("background-menu-color", m_backgroundMenuColor.name());
    writer->writeTextElement("text-menu-color", m_textMenuColor.name());
    writer->writeTextElement("selected-menu-color", m_selectedMenuColor.name());
    writer->writeTextElement("background-selected-menu-color", m_backgroundSelectedMenuColor.name());
    writer->writeTextElement("menu-border-radius-top-left", QString("%1").arg(m_menuBorderRadiusTopLeft));
    writer->writeTextElement("menu-border-radius-top-right", QString("%1").arg(m_menuBorderRadiusTopRight));
    writer->writeTextElement("menu-border-radius-bottom-left", QString("%1").arg(m_menuBorderRadiusBottomLeft));
    writer->writeTextElement("menu-border-radius-bottom-right", QString("%1").arg(m_menuBorderRadiusBottomRight));
    writer->writeTextElement("menu-border-width", QString("%1").arg(m_menuBorderWidth));
    writer->writeTextElement("menu-border-color", m_menuBorderColor.name());
    writer->writeEndElement();
}

void ApplicationsMenuApplet::showContextMenu(const QPoint& point)
{

    QMenu menu;
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure..."), this, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-desktop"), tr("Configure Panel"), PanelApplication::instance(), SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure applets"), m_panelWindow, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("remove"), tr("Remove applet"), this, SLOT(removeApplet()));
    menu.addAction(QIcon::fromTheme("system-software-update"), tr("Update QtPanel from Git"), this, SLOT(updateQtpanelFromGit()));
    menu.addAction(QIcon::fromTheme("application-exit"), tr("Quit panel"), QApplication::instance(), SLOT(quit()));
    menu.exec(point);
}

void ApplicationsMenuApplet::layoutChanged()
{
    m_textItem->setPos(8 + m_pixmapItem->boundingRect().size().width(), m_panelWindow->textBaseLine());
}

bool ApplicationsMenuApplet::isHighlighted()
{
    return m_menuOpened || Applet::isHighlighted();
}

bool ApplicationsMenuApplet::xmlRead()
{
    if (!m_xmlConfigReader.xmlOpen()) {
        MyDBG("Error opening file.");
#ifndef __DEBUG__
        qDebug("Error opening file.");
#endif
        return false;
    }
    while (!m_xmlConfigReader.atEnd()) {
        if (m_xmlConfigReader.hasError()) {
            m_xmlConfigReader.xmlErrorString();
            m_xmlConfigReader.xmlClose();
            return false;
        }
        while (m_xmlConfigReader.readNextStartElement())
            if (m_xmlConfigReader.name() == "applet")
            while (m_xmlConfigReader.readNextStartElement())
                if (m_xmlConfigReader.name() == "type")
                    if (m_xmlConfigReader.readElementText() == getNameApplet())
                        while (m_xmlConfigReader.readNextStartElement())
                            if (m_xmlConfigReader.name() == "config")
                                while (m_xmlConfigReader.readNextStartElement())
                                    if (m_xmlConfigReader.name() == "menu-icon")
                                        xmlReadMenuIcon();
                                    else if (m_xmlConfigReader.name() == "opacity")
                                        xmlReadOpacity();
                                    else if (m_xmlConfigReader.name() == "background-menu-color")
                                        xmlReadBackgroundMenuColor();
                                    else if (m_xmlConfigReader.name() =="text-menu-color")
                                        xmlReadTextMenuColor();
                                    else if (m_xmlConfigReader.name() =="selected-menu-color")
                                        xmlReadSelectedMenuColor();
                                    else if (m_xmlConfigReader.name() =="background-selected-menu-color")
                                        xmlReadBackgroundSelectedMenuColor();
                                    else if (m_xmlConfigReader.name() == "menu-border-radius-top-left")
                                        xmlReadMenuBorderRadiusTopLeft();
                                    else if (m_xmlConfigReader.name() == "menu-border-radius-top-right")
                                        xmlReadMenuBorderRadiusTopRight();
                                    else if (m_xmlConfigReader.name() == "menu-border-radius-bottom-left")
                                        xmlReadMenuBorderRadiusBottomLeft();
                                    else if (m_xmlConfigReader.name() == "menu-border-radius-bottom-right")
                                        xmlReadMenuBorderRadiusBottomRight();
                                    else if (m_xmlConfigReader.name() == "menu-border-width")
                                        xmlReadMenuBorderWidth();
                                    else if (m_xmlConfigReader.name() == "menu-border-color")
                                        xmlReadMenuBorderColor();
    }
    m_xmlConfigReader.xmlClose();
    return true;
}

void ApplicationsMenuApplet::xmlReadMenuIcon()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "menu-icon");
    MyDBG(m_xmlConfigReader.name().toString());
    m_icon = QIcon::fromTheme(m_xmlConfigReader.readElementText(), QIcon());
    MyDBG(m_icon.name());
}

void ApplicationsMenuApplet::xmlReadOpacity()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "opacity");
    MyDBG(m_xmlConfigReader.name().toString());
    m_opacity = m_xmlConfigReader.readElementText().toDouble();
    MyDBG(m_opacity);
}

void ApplicationsMenuApplet::xmlReadBackgroundMenuColor()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "background-menu-color");
    MyDBG(m_xmlConfigReader.name().toString());
    m_backgroundMenuColor = QColor(m_xmlConfigReader.readElementText());
    MyDBG(m_backgroundMenuColor);
}

void ApplicationsMenuApplet::xmlReadTextMenuColor()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "text-menu-color");
    MyDBG(m_xmlConfigReader.name().toString());
    m_textMenuColor = QColor(m_xmlConfigReader.readElementText());
    MyDBG(m_textMenuColor);
}

void ApplicationsMenuApplet::xmlReadSelectedMenuColor()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "selected-menu-color");
    MyDBG(m_xmlConfigReader.name().toString());
    m_selectedMenuColor = QColor(m_xmlConfigReader.readElementText());
    MyDBG(m_selectedMenuColor);
}

void ApplicationsMenuApplet::xmlReadBackgroundSelectedMenuColor()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "background-selected-menu-color");

    MyDBG(m_xmlConfigReader.name().toString());

    m_backgroundSelectedMenuColor = QColor(m_xmlConfigReader.readElementText());

    MyDBG(m_backgroundSelectedMenuColor);

}

void ApplicationsMenuApplet::xmlReadMenuBorderRadiusTopLeft()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "menu-border-radius-top-left");

    MyDBG(m_xmlConfigReader.name().toString());

    m_menuBorderRadiusTopLeft = m_xmlConfigReader.readElementText().toInt();

    MyDBG(m_menuBorderRadiusTopLeft);

}

void ApplicationsMenuApplet::xmlReadMenuBorderRadiusTopRight()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "menu-border-radius-top-right");

    MyDBG(m_xmlConfigReader.name().toString());

    m_menuBorderRadiusTopRight = m_xmlConfigReader.readElementText().toInt();

    MyDBG(m_menuBorderRadiusTopRight);

}

void ApplicationsMenuApplet::xmlReadMenuBorderRadiusBottomLeft()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "menu-border-radius-bottom-left");

    MyDBG(m_xmlConfigReader.name().toString());

    m_menuBorderRadiusBottomLeft = m_xmlConfigReader.readElementText().toInt();

    MyDBG(m_menuBorderRadiusBottomLeft);

}

void ApplicationsMenuApplet::xmlReadMenuBorderRadiusBottomRight()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "menu-border-radius-bottom-right");

    MyDBG(m_xmlConfigReader.name().toString());

    m_menuBorderRadiusBottomRight = m_xmlConfigReader.readElementText().toInt();

    MyDBG(m_menuBorderRadiusBottomRight);

}

void ApplicationsMenuApplet::xmlReadMenuBorderWidth()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "menu-border-width");

    MyDBG(m_xmlConfigReader.name().toString());

    m_menuBorderWidth = m_xmlConfigReader.readElementText().toInt();

    MyDBG(m_menuBorderWidth);

}

void ApplicationsMenuApplet::xmlReadMenuBorderColor()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "menu-border-color");

    MyDBG(m_xmlConfigReader.name().toString());

    m_menuBorderColor = QColor(m_xmlConfigReader.readElementText());

    MyDBG(m_menuBorderColor);

}

void ApplicationsMenuApplet::actionTriggered()
{
    DesktopApplications::instance()->launch(static_cast<QAction*>(sender())->data().toString());
}

void ApplicationsMenuApplet::applicationUpdated(const DesktopApplication& app)
{
    applicationRemoved(app.path());

    if(app.isNoDisplay())
        return;

    QAction* action = new QAction(m_menu);
    action->setIconVisibleInMenu(true);
    action->setData(app.path());
    action->setText(app.name());
    action->setIcon(QIcon(QPixmap::fromImage(app.iconImage())));

    connect(action, SIGNAL(triggered()), this, SLOT(actionTriggered()));

    // Add to relevant menu.
    int subMenuIndex = m_subMenus.size() - 1; // By default put it in "Other".
    for(int i = 0; i < m_subMenus.size() - 1; i++) // Without "Other".
    {
        if(app.categories().contains(m_subMenus[i].category()))
        {
            subMenuIndex = i;
            break;
        }
    }

    QMenu* menu = m_subMenus[subMenuIndex].menu();
    QList<QAction*> actions = menu->actions();
    QAction* before = nullptr;
    for(int i = 0; i < actions.size(); i++)
    {
        if(actions[i]->text().compare(action->text(), Qt::CaseInsensitive) > 0)
        {
            before = actions[i];
            break;
        }
    }

    if(menu->actions().isEmpty())
    {
        QList<QAction*> actions = m_menu->actions();
        QAction* before = nullptr;
        for(int i = 0; i < actions.size(); i++)
        {
            if(actions[i]->text().compare(menu->title(), Qt::CaseInsensitive) > 0)
            {
                before = actions[i];
                break;
            }
        }

        m_menu->insertMenu(before, menu);
    }

    menu->insertAction(before, action);


    m_actions[app.path()] = action;
}

void ApplicationsMenuApplet::applicationRemoved(const QString& path)
{
    if(m_actions.contains(path))
    {
        delete m_actions[path];
        m_actions.remove(path);
    }

    for(int i = 0; i < m_subMenus.size(); i++)
    {
        if(m_subMenus[i].menu()->actions().isEmpty())
            m_menu->removeAction(m_subMenus[i].menu()->menuAction());
    }
}

void ApplicationsMenuApplet::buttonIconClicked()
{
    QString namefile = QFileDialog::getOpenFileName(nullptr, tr("Load Icon"), "/usr/share/icons/"+m_icon.themeName(), tr("Icons (*.png *.svg *.xpm *.ico)"));
    if (!namefile.isNull()) {
        QString name;
        name = QFileInfo(namefile).baseName();
        QIcon icon = QIcon::fromTheme(name);
        m_settingsUi->menu_icon->setIcon(icon);
        m_settingsUi->menu_icon->setText(icon.name());
    }
}

void ApplicationsMenuApplet::updateQtpanelFromGit()
{

    MyDBG("Update Qtpanel from git repository.");

    QString command = "/usr/bin/env bash -c \"sudo rm -R -f /tmp/qtpanel && git clone http://bitbucket.org/mortaromarcello/qtpanel.git -b devel /tmp/qtpanel && cd /tmp/qtpanel && sudo ./build.sh && cd .. && sudo rm -R -f /tmp/qtpanel\"";
    QProcess proc;
    RunProcess mb(&proc);
    proc.start(command);
    if (mb.exec() == QDialog::Accepted) {
        QMessageBox::information(0, tr("Information"), tr("Get out of the session to apply updates."));
    }
}

void ApplicationsMenuApplet::showConfigurationDialog()
{
    QDialog dialog;
    QStringList colors = QColor::colorNames();
    QList <QColor> colorList;
    for (int i = 0; i < colors.size(); i++)
        colorList << QColor(colors.at(i));
    m_settingsUi->setupUi(&dialog);
    m_settingsUi->menu_icon->setIcon(m_icon);
    m_settingsUi->menu_icon->setText(m_icon.name());
    QObject::connect(m_settingsUi->menu_icon, SIGNAL(clicked()), this, SLOT(buttonIconClicked()));
    m_settingsUi->opacity->setRange(0.0, 1.0);
    m_settingsUi->opacity->setSingleStep(0.1);
    m_settingsUi->opacity->setValue(m_opacity);
    m_settingsUi->background_menu_color->addItems(colors);
    m_settingsUi->background_menu_color->setCurrentIndex(colorList.indexOf(m_backgroundMenuColor));
    m_settingsUi->text_menu_color->addItems(colors);
    m_settingsUi->text_menu_color->setCurrentIndex(colorList.indexOf(m_textMenuColor));
    m_settingsUi->selected_menu_color->addItems(colors);
    m_settingsUi->selected_menu_color->setCurrentIndex(colorList.indexOf(m_selectedMenuColor));
    m_settingsUi->background_selected_menu_color->addItems(colors);
    m_settingsUi->background_selected_menu_color->setCurrentIndex(colorList.indexOf(m_backgroundSelectedMenuColor));
    m_settingsUi->menu_border_radius_top_left->setValue(m_menuBorderRadiusTopLeft);
    m_settingsUi->menu_border_radius_top_right->setValue(m_menuBorderRadiusTopRight);
    m_settingsUi->menu_border_radius_bottom_left->setValue(m_menuBorderRadiusBottomLeft);
    m_settingsUi->menu_border_radius_bottom_right->setValue(m_menuBorderRadiusBottomRight);
    m_settingsUi->menu_border_width->setValue(m_menuBorderWidth);
    m_settingsUi->menu_border_color->addItems(colors);
    m_settingsUi->menu_border_color->setCurrentIndex(colorList.indexOf(m_menuBorderColor));
    if(dialog.exec() == QDialog::Accepted) {
        m_icon = m_settingsUi->menu_icon->icon();
        m_pixmapItem->setPixmap(m_icon.pixmap(adjustHardcodedPixelSize(24), adjustHardcodedPixelSize(24)));
        m_opacity = m_settingsUi->opacity->value();
        setOpacity(m_opacity);
        m_backgroundMenuColor = QColor(m_settingsUi->background_menu_color->currentText());
        m_textMenuColor = QColor(m_settingsUi->text_menu_color->currentText());
        m_selectedMenuColor = QColor(m_settingsUi->selected_menu_color->currentText());
        m_backgroundSelectedMenuColor = QColor(m_settingsUi->background_selected_menu_color->currentText());
        m_menuBorderRadiusTopLeft = m_settingsUi->menu_border_radius_top_left->value();
        m_menuBorderRadiusTopRight = m_settingsUi->menu_border_radius_top_right->value();
        m_menuBorderRadiusBottomLeft = m_settingsUi->menu_border_radius_bottom_left->value();
        m_menuBorderRadiusBottomRight = m_settingsUi->menu_border_radius_bottom_right->value();
        m_menuBorderWidth = m_settingsUi->menu_border_width->value();
        m_menuBorderColor = QColor(m_settingsUi->menu_border_color->currentText());
        m_menu->setStyleSheet(QString(menuStyleSheet)
            .arg(adjustHardcodedPixelSize(m_menuBorderWidth))               // 1
            .arg(m_menuBorderColor.name())                                  // 2
            .arg(adjustHardcodedPixelSize(m_menuBorderRadiusTopLeft))       // 3
            .arg(adjustHardcodedPixelSize(m_menuBorderRadiusTopRight))      // 4
            .arg(adjustHardcodedPixelSize(m_menuBorderRadiusBottomLeft))    // 5
            .arg(adjustHardcodedPixelSize(m_menuBorderRadiusBottomRight))   // 6
            .arg(m_backgroundMenuColor.name())                              // 7
            .arg(adjustHardcodedPixelSize(24))                              // 8
            .arg(m_textMenuColor.name())                                    // 9
            .arg(adjustHardcodedPixelSize(32))                              // 10
            .arg(adjustHardcodedPixelSize(16))                              // 11
            .arg(adjustHardcodedPixelSize(2))                               // 12
            .arg(adjustHardcodedPixelSize(2))                               // 13
            .arg(m_backgroundSelectedMenuColor.name())                              // 14
            .arg(m_selectedMenuColor.name())                        // 15
            .arg(adjustHardcodedPixelSize(2))                               // 16
        );
        m_configChanged = true;
    }
}

void ApplicationsMenuApplet::mousePressEvent(QGraphicsSceneMouseEvent* event)
{

    MyDBG("MousePressEvent");

    if(isUnderMouse()) {
        if(event->button() == Qt::LeftButton)
        {
            // FIXME: Workaround.
            // For some weird reason, if clicked() function is called directly, and menu is opened,
            // this item will receive hover enter event on menu close. But it shouldn't (mouse is outside).
            // Probably somehow related to taking a mouse grab when one is already active.
            QTimer::singleShot(1, this, SLOT(clicked()));
        }
        if (event->buttons() == Qt::RightButton)
        showContextMenu(localToScreen(QPoint(0, 0)));
    }
}

void ApplicationsMenuApplet::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
}

void ApplicationsMenuApplet::setOpacity(qreal opacity)
{
    m_menu->setWindowOpacity(m_opacity);
    for (int i = 0; i < m_subMenus.size(); i++) {
        SubMenu menu = m_subMenus.at(i);
        menu.menu()->setWindowOpacity(m_opacity);
    }
}
