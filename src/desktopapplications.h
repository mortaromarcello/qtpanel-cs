#ifndef DESKTOPAPPLICATIONS_H
#define DESKTOPAPPLICATIONS_H

#include <QMetaType>
#include <QObject>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QMap>
#include <QStringList>
#include <QDateTime>
#include <QImage>

class DesktopApplication
{
public:
    DesktopApplication()
        : m_isNoDisplay(false), m_terminal(false)
    {
    }

    bool init(const QString& path);

    bool exists() const;

    bool needUpdate() const;

    void launch() const;

    const bool isNoDisplay() const { return m_isNoDisplay; }
    const QString& path() const { return m_path; }
    const QDateTime& lastUpdated() const { return m_lastUpdated; }
    const QString& name() const { return m_name; }
    const QString& exec() const { return m_exec; }
    const QString& iconName() const { return m_iconName; }
    void setIconImage(const QImage& iconImage) { m_iconImage = iconImage; }
    const QImage& iconImage() const { return m_iconImage; }
    const QStringList& categories() const { return m_categories; }
    const QString& type() const { return m_type; }
    const QString& genericName() const { return m_genericName; }
    const QString& comment() const { return m_comment; }
    const bool terminal() const { return m_terminal; }
    const QString& mimeType() const { return m_mimeType; }
    const QString& workPath() const { return m_workPath; }

private:
    QString m_path;
    QDateTime m_lastUpdated;

    bool m_isNoDisplay;
    QString m_name;
    QString m_exec;
    QString m_iconName;
    QImage m_iconImage;
    QStringList m_categories;
    QString m_type;
    QString m_genericName;
    QString m_comment;
    bool m_terminal;
    QString m_mimeType;
    QString m_workPath;
};

Q_DECLARE_METATYPE(DesktopApplication)

class QDir;
class QTimer;
class QFileSystemWatcher;

class DesktopApplications: public QThread
{
    CS_OBJECT(DesktopApplications)
public:
    DesktopApplications();
    ~DesktopApplications();

    static DesktopApplications* instance() { return m_instance; }

    QList<DesktopApplication> applications();
    DesktopApplication applicationFromPath(const QString& path);
    void launch(const QString& path);

public:
    CS_SIGNAL_1(Public, void applicationUpdated(const DesktopApplication & app))
    CS_SIGNAL_2(applicationUpdated,app) 
    CS_SIGNAL_1(Public, void applicationRemoved(const QString & path))
    CS_SIGNAL_2(applicationRemoved,path) 

protected:
    void run();

private :
    CS_SLOT_1(Private, void directoryChanged(const QString & path))
    CS_SLOT_2(directoryChanged) 
    CS_SLOT_1(Private, void fileChanged(const QString & path))
    CS_SLOT_2(fileChanged) 
    CS_SLOT_1(Private, void refresh())
    CS_SLOT_2(refresh) 

private:
    void traverse(const QDir& dir);

    static DesktopApplications* m_instance;

    QTimer* m_updateTimer;
    QFileSystemWatcher* m_watcher;
    QMutex m_applicationsMutex;
    QMap<QString, DesktopApplication> m_applications;
    QMutex m_tasksMutex;
    QWaitCondition m_tasksWaitCondition;
    bool m_abortWorker;
    QStringList m_fileTasks;
    QStringList m_imageTasks;
};

#endif
