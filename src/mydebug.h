#ifndef MYDEBUG_H
#define MYDEBUG_H
#include <QDebug>
#include "qtpanelconfig.h"

#define __DEBUG__

#ifdef __DEBUG__
#define MyDBG( args ) ( qDebug() << "File:" << __FILE__ << "Line:" << __LINE__ << "Function:" << __PRETTY_FUNCTION__ << "\n\t->" << args )
#else
#define MyDBG( args )
#endif

#endif //MYDEBUG_H__
