#include "clockapplet.h"

#include <QTimer>
#include <QDateTime>
#include <QGraphicsScene>
#include <QToolTip>
#include <QCalendarWidget>
#include <QMenu>
#include <QProcess>
#include <QMessageBox>
#include "textgraphicsitem.h"
#include "panelwindow.h"
#ifdef USE_ORGANIZER
#include "organizer.h"
#endif
#include "panelapplication.h"
#include "ui_appletclocksettings.h"

ClockApplet::ClockApplet(PanelWindow* panelWindow)
    : Applet(panelWindow),
    m_timer(new QTimer()),
    m_textItem(new TextGraphicsItem(this)),
#ifdef USE_ORGANIZER
    m_organizer(new Organizer()),
    m_useOrganizer(false),
#endif
    m_timeFormat("H:m"),
    m_settingsUi(new Ui::AppletClockSettings()),
    m_timePowerOff(QTime()),
    m_powerOffEnabled(false)
{
    if (!xmlRead()) {
        qWarning("Don't read configuration applet file.");
    }
    m_timer->setSingleShot(true);
    connect(m_timer, SIGNAL(timeout()), this, SLOT(updateContent()));
    m_textItem->setColor(Qt::white);
    m_textItem->setFont(m_panelWindow->font());
#ifdef USE_ORGANIZER
    m_organizer->setWindowFlags(Qt::FramelessWindowHint);
    m_organizer->setStyleSheet("QMainWindow {border: 1px solid black}");
    m_organizer->resize(m_organizer->sizeHint());
#endif
}

ClockApplet::~ClockApplet()
{
    delete m_textItem;
    delete m_timer;
#ifdef USE_ORGANIZER
    delete m_organizer;
#endif
}

void ClockApplet::xmlWrite(XmlConfigWriter* writer)
{
    writer->writeStartElement("config");
#ifdef USE_ORGANIZER
    writer->writeTextElement("useorganizer", (m_useOrganizer) ? "true" : "false");
#endif
    writer->writeTextElement("timeformat", QString("%1").arg(m_timeFormat));
    writer->writeTextElement("poweroffenabled", (m_powerOffEnabled) ? "true" : "false");
    writer->writeTextElement("timepoweroff", m_timePowerOff.toString());
    writer->writeEndElement();
}

bool ClockApplet::init()
{
    updateContent();
    setInteractive(true);
    return true;
}

void ClockApplet::layoutChanged()
{
    m_textItem->setPos((m_size.width() - m_textItem->boundingRect().size().width())/2.0, m_panelWindow->textBaseLine());
}

void ClockApplet::mousePressEvent(QGraphicsSceneMouseEvent* event)
{

    MyDBG("MousePressEvent");

    if (event->buttons() == Qt::RightButton)
        showContextMenu(localToScreen(QPoint(0, 0)));
#ifdef USE_ORGANIZER
    if (event->buttons() == Qt::LeftButton && m_useOrganizer) {
        if (m_organizer->isHidden()) {
            int x, y;
            x = pos().x()+size().width()-m_organizer->width();
            x = (x < 0) ? 0 : x;
            y = (m_panelWindow->verticalAnchor() == PanelWindow::Top) ? size().height():localToScreen(pos().toPoint()).y() - m_organizer->height()-size().height();
            QPoint point = QPoint(x, y);
            m_organizer->move(point);
            m_organizer->show();
        }
        else m_organizer->hide();
    }
#endif
}

void ClockApplet::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
}

bool ClockApplet::xmlRead()
{
    if (!m_xmlConfigReader.xmlOpen()) {
        MyDBG("Error opening file.");
#ifndef __DEBUG__
        qDebug("Error opening file.");
#endif
        return false;
    }
    while (!m_xmlConfigReader.atEnd()) {
        if (m_xmlConfigReader.hasError()) {
            m_xmlConfigReader.xmlErrorString();
            m_xmlConfigReader.xmlClose();
            return false;
        }
        while (m_xmlConfigReader.readNextStartElement())
            if (m_xmlConfigReader.name() == "applet")
            while (m_xmlConfigReader.readNextStartElement())
                if (m_xmlConfigReader.name() == "type")
                    if (m_xmlConfigReader.readElementText() == getNameApplet())
                        while (m_xmlConfigReader.readNextStartElement())
                            if (m_xmlConfigReader.name() == "config")
                                while (m_xmlConfigReader.readNextStartElement())
#ifdef USE_ORGANIZER
                                    if (m_xmlConfigReader.name() == "useorganizer")
                                        xmlReadUseOrganizer();
                                    else
#endif
                                        if (m_xmlConfigReader.name() == "timeformat")
                                        xmlReadTimeFormat();
                                    else if (m_xmlConfigReader.name() == "poweroffenabled")
                                        xmlReadPowerOffEnabled();
                                    else if (m_xmlConfigReader.name() == "timepoweroff")
                                        xmlReadTimePowerOff();
    }
    m_xmlConfigReader.xmlClose();
    return true;
}

#ifdef USE_ORGANIZER
void ClockApplet::xmlReadUseOrganizer()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "useorganizer");

    MyDBG(m_xmlConfigReader.name().toString());

    m_useOrganizer = (m_xmlConfigReader.readElementText() == "true") ? true : false;
}
#endif

void ClockApplet::xmlReadTimeFormat()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "timeformat");

    MyDBG(m_xmlConfigReader.name().toString());

    m_timeFormat = m_xmlConfigReader.readElementText();
}

void ClockApplet::xmlReadPowerOffEnabled()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "poweroffenabled");

    MyDBG(m_xmlConfigReader.name().toString());

    m_powerOffEnabled = (m_xmlConfigReader.readElementText() == "true") ? true : false;
}

void ClockApplet::xmlReadTimePowerOff()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "timepoweroff");

    MyDBG(m_xmlConfigReader.name().toString());

    m_timePowerOff = QTime::fromString(m_xmlConfigReader.readElementText());
}

void ClockApplet::updateContent()
{
    QDate dateNow = QDate::currentDate();
    m_textToolTip = dateNow.toString();
    QTime timeNow = QTime::currentTime();
    if (m_powerOffEnabled && (timeNow.hour() == m_timePowerOff.hour() && timeNow.minute() == m_timePowerOff.minute())) {
        qApp->quit();
        QProcess::startDetached("gksudo \"shutdown -h -P now\"");
    }
    m_text = timeNow.toString(m_timeFormat);
    m_textItem->setText(m_text);
    scheduleUpdate();
}

QSize ClockApplet::desiredSize()
{
    return QSize(m_textItem->boundingRect().width() + 8, m_textItem->boundingRect().height() + 8);
}

void ClockApplet::showConfigurationDialog()
{
    QDialog dialog;
    QStringList formats = QStringList() << "h:m" << "h:mm" << "hh:mm" << "H:m" << "H:mm" << "HH:mm" << "h:m:s" << "h:m:ss" << "h:mm:ss" << "hh:mm:ss" << "h:m ap" << "h:mm ap" << "hh:mm ap" << "h:m:s ap" << "h:m:ss ap" << "h:mm:ss ap" << "hh:mm:ss ap";
    m_settingsUi->setupUi(&dialog);
#ifdef USE_ORGANIZER
    m_settingsUi->useorganizer->setChecked(m_useOrganizer);
#else
    m_settingsUi->useorganizer->hide();
#endif
    m_settingsUi->timeFormat->addItems(formats);
    m_settingsUi->timeFormat->setCurrentIndex(formats.indexOf(m_timeFormat));
    m_settingsUi->setDateTime->setDateTime(QDateTime::currentDateTime());
    m_settingsUi->setDateTime->setEnabled(m_settingsUi->checkBoxDateTime->isChecked());
    m_settingsUi->checkBoxSetPowerOff->setChecked(m_powerOffEnabled);
    m_settingsUi->timePowerOff->setEnabled(m_powerOffEnabled);
    m_settingsUi->timePowerOff->setTime(m_timePowerOff);
    if(dialog.exec() == QDialog::Accepted) {
#ifdef USE_ORGANIZER
        m_useOrganizer = m_settingsUi->useorganizer->isChecked();
#endif
        m_timeFormat = m_settingsUi->timeFormat->currentText();

        if (m_settingsUi->checkBoxDateTime->isChecked())
        {
            QProcess::startDetached(QString("gksudo date %1").arg(m_settingsUi->setDateTime->dateTime().toString("MMddhhmmyy.ss")));
        }
        m_powerOffEnabled = m_settingsUi->checkBoxSetPowerOff->isChecked();
        if (m_powerOffEnabled)
        {
            m_timePowerOff = m_settingsUi->timePowerOff->time();
        }
        // restart:
        //qApp->quit();
        //QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
        m_configChanged = true;
        updateContent();
    }
}

void ClockApplet::scheduleUpdate()
{
    m_timer->setInterval(1000 - QDateTime::currentDateTime().time().msec());
    m_timer->start();
}

void  ClockApplet::showContextMenu(const QPoint& point)
{
    QMenu menu;
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure..."), this, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-desktop"), tr("Configure Panel"), PanelApplication::instance(), SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure applets"), m_panelWindow, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("remove"), tr("Remove applet"), this, SLOT(removeApplet()));
    menu.exec(point);
}
