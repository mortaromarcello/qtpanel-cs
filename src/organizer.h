#ifndef ORGANIZER_H
#define ORGANIZER_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSettings>
#include <QStackedWidget>
#include <QTableWidget>
#include <QTextEdit>
#include <QLabel>
#include <QItemDelegate>
#include <QFocusEvent>
#include <QLineEdit>
#include <phonon>
#include <QComboBox>
#include <QPushButton>
#include <QFontComboBox>
#include "calendarworkinghours.h"

class QToolButton;
class Organizer;

//---------------------------QBalloonTip-------------------------------
class QBalloonTip : public QWidget
{
public:
    static QWidget *showBalloon(const QString& title, const QString& msg, int timeout=0, bool showArrow = true);
    static void hideBalloon();

private:
    QBalloonTip(const QString& title, const QString& msg,QWidget* parent=0);
    ~QBalloonTip();
    void balloon(int, bool);

protected:
    void paintEvent(QPaintEvent *);
    void resizeEvent(QResizeEvent *);
    void mousePressEvent(QMouseEvent *e);
    void timerEvent(QTimerEvent *e);

private:
    QPixmap pixmap;
    int timerId;
};

//-----------------------CQTableWidget-------------------------------
class CQTableWidget:public QTableWidget
{
    CS_OBJECT(CQTableWidget)

public:
    bool isBeingEdited() {(this->state() == QAbstractItemView::EditingState) ? true : false;}

public:
    CS_SIGNAL_1(Public, void focusLost())
    CS_SIGNAL_2(focusLost) 

protected:
    void focusOutEvent(QFocusEvent * event);
    void keyPressEvent (QKeyEvent * event);
};

//Custom QTableWidget for the same thing.

class CQTextEdit:public QTextEdit
{
    CS_OBJECT(CQTextEdit)

public:
    CS_SIGNAL_1(Public, void focusLost())
    CS_SIGNAL_2(focusLost) 

protected:
    void focusOutEvent(QFocusEvent * event);
    /*void dragEnterEvent(QDragEnterEvent *event);
    //void dropEvent(QDropEvent *e);*/
    void insertFromMimeData(const QMimeData *source);
    bool canInsertFromMimeData(const QMimeData *source) const;
};

//LineEdit with clear button on the right.
class LineEdit : public QLineEdit
{
    CS_OBJECT(LineEdit)

public:
    LineEdit(Organizer *parent = 0);

protected:
    void resizeEvent(QResizeEvent *);

private :
    CS_SLOT_1(Private, void updateCloseButton(const QString & text))
    CS_SLOT_2(updateCloseButton) 
    CS_SLOT_1(Private, void clearAndSetDateBack())
    CS_SLOT_2(clearAndSetDateBack) 
private:
    QToolButton *clearButton;
    Organizer *Parent;
};

//Delegate for the schedule
class ScheduleDelegate : public QItemDelegate
{
    CS_OBJECT(ScheduleDelegate)
public:
    ScheduleDelegate(QObject *parent = 0);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model,const QModelIndex &index) const;
    void updateEditorGeometry(QWidget *editor,const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

// class organizer
class Organizer: public QMainWindow
{
    CS_OBJECT(Organizer)
public:
    Organizer();
    ~Organizer();
    bool init();
    CalendarWorkingHours* calendar() {return m_calendar;}

public :
    CS_SLOT_1(Public, void updateDay())
    CS_SLOT_2(updateDay)
    CS_SLOT_1(Public, void timeout() {remind(0, 0);})
    CS_SLOT_2(timeout)
    CS_SLOT_1(Public, void remind(int, int))
    CS_SLOT_2(remind)
    CS_SLOT_1(Public, void playSound())
    CS_SLOT_2(playSound) 
    CS_SLOT_1(Public, void saveSchedule())
    CS_SLOT_2(saveSchedule) 
    CS_SLOT_1(Public, void loadSchedule())
    CS_SLOT_2(loadSchedule) 
    CS_SLOT_1(Public, void saveJournal())
    CS_SLOT_2(saveJournal) 
    CS_SLOT_1(Public, void loadJournal())
    CS_SLOT_2(loadJournal) 
    CS_SLOT_1(Public, void updateCalendar())
    CS_SLOT_2(updateCalendar) 
    CS_SLOT_1(Public, void modifyText())
    CS_SLOT_2(modifyText)
    CS_SLOT_1(Public, void currentFontChanged(const QFont&) {setFont();})
    CS_SLOT_2(currentFontChanged)
    CS_SLOT_1(Public, void setFont())
    CS_SLOT_2(setFont) 
    CS_SLOT_1(Public, void insertRowToEnd())
    CS_SLOT_2(insertRowToEnd) 
    CS_SLOT_1(Public, void deleteRow())
    CS_SLOT_2(deleteRow) 
    CS_SLOT_1(Public, void searchPrev())
    CS_SLOT_2(searchPrev) 
    CS_SLOT_1(Public, void searchNext())
    CS_SLOT_2(searchNext) 
    CS_SLOT_1(Public, void search())
    CS_SLOT_2(search) 
    CS_SLOT_1(Public, void saveCalendarColumnWidths())
    CS_SLOT_2(saveCalendarColumnWidths) 
    CS_SLOT_1(Public, void setCalendarColumnWidths())
    CS_SLOT_2(setCalendarColumnWidths) 
private:
    void readSettings();
    void saveSettings();
    void addItems();
    QWidget* getCalendarPage();
    void emptyTable();
    bool tableExistsDB(QString &tablename);
    QSqlDatabase m_db;
    QString m_nameDatabase;
    QSettings * m_settings;
    QStackedWidget* m_mainWid;
    CalendarWorkingHours* m_calendar;
    CQTableWidget* m_tableWid;
    CQTextEdit* m_journal;
    Qt::DayOfWeek m_firstDayOfWeek;
    int m_rowCount;
    // fonts
    QFont m_font;
    QFontComboBox* m_fontBox;
    QComboBox* m_comboSize;
    QPushButton* m_buttonBold;
    QPushButton* m_buttonItalic;
    QPushButton* m_buttonUnderline;
    // search
    QLineEdit* m_searchField;
    QString m_oldSearchString;
    int m_searchCurrentIndex;
    QList<QDate> m_dateList;
    //
    QString m_dateFormat;
    QString m_selDate;
    QLabel* m_noteLabel;
    QLabel* m_day;
    ScheduleDelegate m_scheduleDelegate;
    // timer
    QTimer* m_timer;
    unsigned long long int m_timeout;
    // sound
    bool m_sound;
    Phonon::MediaObject* m_mediaObject;
    Phonon::AudioOutput* m_audioOutput;
    Phonon::Path m_mediaPath;
};


#endif // ORGANIZER_H

