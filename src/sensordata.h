#ifndef SENSORDATA_H
#define SENSORDATA_H

#include <string>
#include <list>
#include <sensors/sensors.h>

class SensorsSubFeature
{
public:
    SensorsSubFeature(const sensors_chip_name *cn, const sensors_feature *feat, const sensors_subfeature *subf);
    std::string getName() {return std::string(m_subfeature->name);}
    int getNumber() {return m_subfeature->number;}
    sensors_subfeature_type getType() {return m_subfeature->type;}
    int getMapping() {return m_subfeature->mapping;}
    unsigned int getFlags() {return m_subfeature->flags;}
    double getValue();
protected:
    const sensors_subfeature *m_subfeature;
    const sensors_feature *m_feature;
    const sensors_chip_name *m_chip_name;
};

class SensorsFeature
{
public:
    SensorsFeature(const sensors_chip_name *cn, const sensors_feature *feat);
    std::list<SensorsSubFeature *> getListSensorsSubFeature() {return m_sensors_subfeature;}
    std::string getName() {return std::string(m_feature->name);}
    int getNumber() {return m_feature->number;}
    sensors_feature_type getType() {return m_feature->type;}
    int getFirstSubfeature() {return m_feature->first_subfeature;}
    int getPadding1() {return m_feature->padding1;}
    std::string getLabel();
protected:
    std::list<SensorsSubFeature *> m_sensors_subfeature;
    const sensors_chip_name *m_chip_name;
    const sensors_feature *m_feature;
};

class SensorsChipName
{
public:
    SensorsChipName(const sensors_chip_name *cn);
    std::list<SensorsFeature *> getListSensorsFeature() {return m_sensors_feature;}
    std::string getPrefix() {return std::string(m_chip_name->prefix);}
    sensors_bus_id getBus() {return m_chip_name->bus;}
    int getAddr() {return m_chip_name->addr;}
    std::string getPath() {return std::string(m_chip_name->path);}
    SensorsFeature *getSensorsFeature(const std::string& label);
protected:
    std::list<SensorsFeature *> m_sensors_feature;
    const sensors_chip_name *m_chip_name;
};

class SensorsData
{
public:
    SensorsData();
    std::list<SensorsChipName *> getListSensorsChipName() {return m_sensors_chip_name;}
    SensorsChipName * getSensorsChipName(const std::string& prefix);
protected:
    std::list<SensorsChipName *> m_sensors_chip_name;
};

#endif
