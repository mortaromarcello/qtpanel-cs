#ifndef DEMOAPPLET_H
#define DEMOAPPLET_H

#include "applet.h"
#include "ui_appletdemosettings.h"

class QGraphicsRectItem;

class DemoApplet: public Applet
{
	CS_OBJECT(DemoApplet)
public:
	DemoApplet(PanelWindow* panelWindow);
	~DemoApplet();
	bool init();
	void xmlWrite(XmlConfigWriter* writer);
	void showContextMenu(const QPoint& point);
	QSize desiredSize();

public :
	CS_SLOT_1(Public, void showConfigurationDialog())
	CS_SLOT_2(showConfigurationDialog) 

protected:
	void layoutChanged();
	void mousePressEvent(QGraphicsSceneMouseEvent* event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
	bool xmlRead();
	void xmlReadColor();

private:
	QGraphicsRectItem* m_rectItem;
	QColor m_color;
	Ui_AppletDemoSettings* m_settingsUi;
};

#endif // DEMOAPPLET_H
