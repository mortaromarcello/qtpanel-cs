#ifndef CLOCKAPPLET_H
#define CLOCKAPPLET_H

#include "applet.h"
#include <QGraphicsSceneMouseEvent>
#include <QTime>

//#define USE_ORGANIZER

class Ui_AppletClockSettings;

#ifdef USE_ORGANIZER
class Organizer;
#endif

class QTimer;
class TextGraphicsItem;

class ClockApplet: public Applet
{
    CS_OBJECT(ClockApplet)
public:
    ClockApplet(PanelWindow* panelWindow);
    ~ClockApplet();
    void xmlWrite(XmlConfigWriter* writer);
    void showContextMenu(const QPoint& point);

    bool init();
    QSize desiredSize();

public :
    CS_SLOT_1(Public, void showConfigurationDialog())
    CS_SLOT_2(showConfigurationDialog) 

protected:
    void layoutChanged();
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    bool xmlRead();
#ifdef USE_ORGANIZER
    void xmlReadUseOrganizer();
#endif
    void xmlReadTimeFormat();
    void xmlReadPowerOffEnabled();
    void xmlReadTimePowerOff();

private :
    CS_SLOT_1(Private, void updateContent())
    CS_SLOT_2(updateContent) 

private:
    void scheduleUpdate();

    QTimer* m_timer;
    QString m_text;
    TextGraphicsItem* m_textItem;
#ifdef USE_ORGANIZER
    Organizer* m_organizer;
    bool m_useOrganizer;
#endif
    QString m_timeFormat;
    Ui_AppletClockSettings *m_settingsUi;
    QTime m_timePowerOff;
    bool m_powerOffEnabled;
};

#endif
