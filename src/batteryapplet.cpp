#include <QToolTip>
#include <QMenu>
#include <QGraphicsSceneMouseEvent>
#include <QProcess>
#include "batteryapplet.h"
#include "dpisupport.h"
#include "ui_appletbatterysettings.h"
#include "qtpanelconfig.h"
#include "panelapplication.h"

BatteryApplet::BatteryApplet(PanelWindow* panelWindow)
    : Applet(panelWindow),
    m_pixmapItem(new QGraphicsPixmapItem(this)),
    m_percBeforeHalt(5),
    m_haltCommand("gksudo \"shutdown -h -P now\""),
    m_delta(adjustHardcodedPixelSize(4)),
    m_settingsUi(new Ui::AppletBatterySettings())
{
    batteryIconMissing = "battery-missing.png";
    batteryIcons << "battery-empty.png" << "battery-caution.png"<< "battery-low.png" << "battery-good.png" << "battery-full.png";
    batteryIconsCharging << "battery-empty-charging.png" << "battery-caution-charging.png"<< "battery-low-charging.png" << "battery-good-charging.png" << "battery-full-charging.png";
    if (!xmlRead()) {
        qWarning("Don't read configuration applet file.");
    }
}

BatteryApplet::~BatteryApplet()
{
    delete m_pixmapItem;
    delete m_settingsUi;
    m_timer.stop();
}

bool BatteryApplet::init()
{
    setInteractive(true);
    if (typeBattery()) {
        connect(&m_timer, SIGNAL(timeout()), this, SLOT(updateInfo()));
        m_timer.start(2000);
        setIcon(percentage());
    }
    else
        setIcon(-1);
    return true;
}

QSize BatteryApplet::desiredSize()
{
    return QSize(adjustHardcodedPixelSize(32), adjustHardcodedPixelSize(32));
}

void BatteryApplet::updateInfo()
{
    Battery::update();
    m_textToolTip = Battery::info();
    setIcon(percentage());
    if (percentage() <= m_percBeforeHalt && !isCharging()) {
        m_timer.stop();
        if (m_panelWindow->isConfigAppletsChanged())
            m_panelWindow->xmlWrite();
        QProcess::startDetached(m_haltCommand);
    }
}

void BatteryApplet::setIcon(int percentage)
{
    if (percentage == -1) {
        m_icon = QIcon(":/images/" + batteryIconMissing);
        m_pixmapItem->setPixmap(m_icon.pixmap(adjustHardcodedPixelSize(24), adjustHardcodedPixelSize(24)));
    }
    int step = 100 / batteryIcons.size();
    for (int i = 0; i < batteryIcons.size(); i++) {
        if (percentage == 100) {
            if (isCharging())
                m_icon = QIcon(":/images/" + batteryIconsCharging.at(batteryIconsCharging.size()-1));
            else
                m_icon = QIcon(":/images/" + batteryIcons.at(batteryIcons.size()-1));
            break;
            }
        if (percentage >= i * step && percentage < (i + 1) * step) {
            if (isCharging())
            m_icon = QIcon(":/images/" + batteryIconsCharging.at(i));
            else
                m_icon = QIcon(":/images/" + batteryIcons.at(i));
            break;
        }
    }
    m_pixmapItem->setPixmap(m_icon.pixmap(adjustHardcodedPixelSize(24), adjustHardcodedPixelSize(24)));
    //
}

void BatteryApplet::layoutChanged()
{
    m_pixmapItem->setOffset(m_delta, m_delta);
}

bool BatteryApplet::xmlRead()
{
    if (!m_xmlConfigReader.xmlOpen()) {
        MyDBG("Error opening file.");
#ifndef __DEBUG__
        qDebug("Error opening file.");
#endif
        return false;
    }
    while (!m_xmlConfigReader.atEnd()) {
        if (m_xmlConfigReader.hasError()) {
            m_xmlConfigReader.xmlErrorString();
            m_xmlConfigReader.xmlClose();
            return false;
        }
        while (m_xmlConfigReader.readNextStartElement())
            if (m_xmlConfigReader.name() == "applet")
            while (m_xmlConfigReader.readNextStartElement())
                if (m_xmlConfigReader.name() == "type")
                    if (m_xmlConfigReader.readElementText() == getNameApplet())
                        while (m_xmlConfigReader.readNextStartElement())
                            if (m_xmlConfigReader.name() == "config")
                                while (m_xmlConfigReader.readNextStartElement())
                                    if (m_xmlConfigReader.name() == "perc-before-halt")
                                        xmlReadPercBeforeHalt();
                                    else if (m_xmlConfigReader.name() == "halt-command")
                                        xmlReadHaltCommand();
    }
    m_xmlConfigReader.xmlClose();
    return true;
}

void BatteryApplet::xmlReadIconMissing()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "icon-missing");

    MyDBG(m_xmlConfigReader.name().toString());

    batteryIconMissing = m_xmlConfigReader.readElementText();

    MyDBG(batteryIconMissing);

}

void BatteryApplet::xmlReadIcons()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "icons");

    MyDBG(m_xmlConfigReader.name().toString());

    batteryIcons = QString(m_xmlConfigReader.readElementText()).split(",");

    MyDBG(batteryIcons);

}

void BatteryApplet::xmlReadIconsCharging()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "icons-charging");

    MyDBG(m_xmlConfigReader.name().toString());

    batteryIconsCharging = QString(m_xmlConfigReader.readElementText()).split(",");

    MyDBG(batteryIconsCharging);

}

void BatteryApplet::xmlReadPercBeforeHalt()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "perc-before-halt");

    MyDBG(m_xmlConfigReader.name().toString());

    m_percBeforeHalt = m_xmlConfigReader.readElementText().toInt();

    MyDBG(m_percBeforeHalt);

}

void BatteryApplet::xmlReadHaltCommand()
{
    Q_ASSERT(m_xmlConfigReader.isStartElement() && m_xmlConfigReader.name() == "halt-command");

    MyDBG(m_xmlConfigReader.name().toString());

    m_haltCommand = m_xmlConfigReader.readElementText();

    MyDBG(m_percBeforeHalt);

}

void BatteryApplet::xmlWrite(XmlConfigWriter* writer)
{
    writer->writeStartElement("config");
    writer->writeTextElement("perc-before-halt", QString("%1").arg(m_percBeforeHalt));
    writer->writeTextElement("halt-command", m_haltCommand);
    writer->writeEndElement();
}

void BatteryApplet::showContextMenu(const QPoint& point)
{
    QMenu menu;
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure..."), this, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-desktop"), tr("Configure Panel"), PanelApplication::instance(), SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("preferences-other"), tr("Configure applets"), m_panelWindow, SLOT(showConfigurationDialog()));
    menu.addAction(QIcon::fromTheme("remove"), tr("Remove applet"), this, SLOT(removeApplet()));
    menu.exec(point);
}

void BatteryApplet::showConfigurationDialog()
{
    QDialog dialog;
    m_settingsUi->setupUi(&dialog);
    m_settingsUi->perc_before_halt->setValue(m_percBeforeHalt);
    m_settingsUi->halt_command->insert(m_haltCommand);
    if(dialog.exec() == QDialog::Accepted) {
        m_percBeforeHalt = m_settingsUi->perc_before_halt->value();
        m_haltCommand = m_settingsUi->halt_command->text();
        m_configChanged = true;
    }
}

void BatteryApplet::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
}

void BatteryApplet::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    if (isUnderMouse())
        showContextMenu(localToScreen(QPoint(0, 0)));
}
