#include "menu.h"
#include "mydebug.h"
#include "xdg.h"
#include <QMenu>
#include <tinyxml2.h>

using namespace tinyxml2;

Menu::Menu(bool is_root, Menu * parent, QString app_dir, QString default_app_dirs, QString directory_dir, QString default_directory_dirs, QString name, Directory directory, QString filename, QString category):
        m_isRootMenu(is_root),
        m_appDir(app_dir),
        m_defaultAppDirs(default_app_dirs),
        m_directoryDir(directory_dir),
        m_defaultDirectoryDirs(default_directory_dirs),
        m_name(name),
        m_directory(directory),
        m_filename(filename),
        m_category(category),
        m_parent(parent)
{
    if (m_isRootMenu)
        m_parent = nullptr;
        if (!m_directory.name().isEmpty())
            m_name = m_directory.name();
}

void Menu::print()
{
    qDebug() << "Is Root =" << isRootMenu() << "\nAppDir =" << appDir() << "\nDefaultAppDirs =" << defaultAppDirs() << "\nDirectoryDir =" << directoryDir() << "\nName =" << name() << "\nDirectory =" << directory().filename() << "\nFilename =" << filename() << "\nCategory =" << category() << "\nMenu* =" << this << "\nMenu* parent =" << parent() << "\nRootMenu =" << getRootMenu() << "\n\n";
    for(Menu* menu: subMenus()) {
        menu->print();
    }
}

Menu* Menu::getRootMenu()
{
    Menu* menu = this;
    while(menu) {
        if(menu->isRootMenu())
            return menu;
        menu = menu->parent();
    }
}

Menu* getSubMenu(Menu* parent, XMLElement* xml_menu)
{
    Menu* result = nullptr;
    if (QString(xml_menu->Value()) != "Menu")
    {
        MyDBG("Error!");
        return result;
    }
    XdgDirectory xdg;
    XMLElement* element;
    XMLElement* sub_menu;
    QString app_dir;
    QString default_app_dirs;
    QString directory_dir;
    QString default_directory_dirs;
    QString name;
    Directory directory;
    QString filename;
    QString category;
    element = xml_menu->FirstChildElement("AppDir");
    if (element) app_dir = element->GetText();
    else app_dir = "";
    element = xml_menu->FirstChildElement("DefaultAppDirs");
    if (element) default_app_dirs = element->GetText();
    else default_app_dirs = "";
    element = xml_menu->FirstChildElement("DirectoryDir");
    if (element) directory_dir = element->GetText();
    else directory_dir = "";
    element = xml_menu->FirstChildElement("DefaultDirectoryDirs");
    if (element) default_directory_dirs = element->GetText();
    else default_directory_dirs = "";
    element = xml_menu->FirstChildElement("Name");
    if (element) name = element->GetText();
    else name = "";
    MyDBG(name);
    element = xml_menu->FirstChildElement("Directory");
    if (element) {
        QString dir = element->GetText();
        if(!dir.isEmpty())  {
            MyDBG(QString(xdg.getXdgDataHome() + QDir::separator() + "desktop-directories"));
            if (QDir(xdg.getXdgDataHome() + QDir::separator() + "desktop-directories").exists()) {
                directory = Directory( xdg.getXdgDataHome() + QDir::separator() + "desktop-directories" + QDir::separator() + dir);
                MyDBG(directory.filename() << directory.name());
            }
            else {
                for(QString path: xdg.getXdgDataDirsList()) {
                    directory = Directory(path + QDir::separator() + "desktop-directories" + QDir::separator() + dir);
                }
            }
        }
    }
    else directory = Directory();
    element = xml_menu->FirstChildElement("Filename");
    if (element) filename = element->GetText();
    else filename = "";
    element = xml_menu->FirstChildElement("Category");
    if (element) category = element->GetText();
    else category = "";
    result = new Menu(false, parent, app_dir, default_app_dirs, directory_dir, default_directory_dirs, name, directory, filename, category);
    MyDBG(result);
    sub_menu = xml_menu->FirstChildElement("Menu");
    MyDBG(sub_menu);
    while(sub_menu) {
        Menu* menu = getSubMenu(result, sub_menu);
        if(menu)
            result->addMenu(menu);
        sub_menu = sub_menu->NextSiblingElement("Menu");
    }
    return result;
}

Menu* createMenu(QString fname)
{
    Menu* result = nullptr;
    XdgDirectory xdg;
    XMLDocument xmlDoc;
    MyDBG("createMenu");
    XMLError eResult = xmlDoc.LoadFile(fname.toLatin1().data());
    if (eResult != XML_SUCCESS)
        MyDBG("XMLError:" << eResult);
    XMLElement* pRoot = xmlDoc.FirstChildElement("Menu");
    
    if (pRoot)
    {
        XMLElement* element;
        XMLElement* xml_menu;
        QString app_dir;
        QString default_app_dirs;
        QString directory_dir;
        QString default_directory_dirs;
        QString name;
        Directory directory;
        QString filename;
        QString category;
        element = pRoot->FirstChildElement("AppDir");
        if (element) app_dir = element->GetText();
        else app_dir = "";
        element = pRoot->FirstChildElement("DefaultAppDirs");
        if (element) default_app_dirs = element->GetText();
        else default_app_dirs = "";
        element = pRoot->FirstChildElement("DirectoryDir");
        if (element) directory_dir = element->GetText();
        else directory_dir = "";
        element = pRoot->FirstChildElement("DefaultDirectoryDirs");
        if (element) default_directory_dirs = element->GetText();
        else default_directory_dirs = "";
        element = pRoot->FirstChildElement("Name");
        if (element) name = element->GetText();
        else name = "";
        MyDBG(name);
        element = pRoot->FirstChildElement("Directory");
        if (element) {
            QString dir = element->GetText();
            if(!dir.isEmpty())  {
                MyDBG(QString(xdg.getXdgDataHome() + QDir::separator() + "desktop-directories"));
                if (QDir(xdg.getXdgDataHome() + QDir::separator() + "desktop-directories").exists()) {
                    directory = Directory( xdg.getXdgDataHome() + QDir::separator() + "desktop-directories" + QDir::separator() + dir);
                    MyDBG(directory.filename() << directory.name());
                }
                else {
                    for(QString path: xdg.getXdgDataDirsList()) {
                        directory = Directory(path + QDir::separator() + "desktop-directories" + QDir::separator() + dir);
                    }
                }
            }
        }
        else directory = Directory();
        element = pRoot->FirstChildElement("Filename");
        if (element) filename = element->GetText();
        else filename = "";
        element = pRoot->FirstChildElement("Category");
        if (element) category = element->GetText();
        else category = "";
        MyDBG(pRoot->Value());
        result = new Menu(true, nullptr, app_dir, default_app_dirs, directory_dir, default_directory_dirs, name, Directory(directory), filename, category);
        xml_menu = pRoot->FirstChildElement("Menu");
        while(xml_menu) {
            Menu* menu = getSubMenu(result, xml_menu);
            if (menu)
                result->addMenu(menu);
            xml_menu = xml_menu->NextSiblingElement("Menu");
        }
    }
    return result;
}

Directory::Directory(QString filename)
{
    m_filename = filename;
    QFile file(m_filename);
    if (!file.exists()) {
        MyDBG("filename not exists.");
        return;
    }
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        MyDBG("Error opening file.");
        return;
    }
    QTextStream in(&file);
    while(!in.atEnd()) {
        QString line = in.readLine();
        if(line[0] == '[')
        {
            if(line.contains("Desktop Entry"))
                continue;
            else
                break; // We only process "Desktop Entry" here.
        }
        if(line[0] == '#')
            continue;
        QStringList list = line.split('=');
        if(list.size() < 2)
            continue;
        QString key = list[0].trimmed();
        QString value = list[1].trimmed();
        if(key == (QString ("Name[%1]").arg(QLocale().name().split('_').first())))
            m_name = value.toUtf8();
        if(key == "Name")
            if (m_name.isEmpty()) m_name = value;
        if(key == (QString("Comment[%1]").arg(QLocale().name().split('_').first())))
            m_comment = value.toUtf8();
        if(key == "Comment")
            m_comment = value;
        if(key == "Icon")
            m_icon = value;
        if(key == "Type")
            m_type = value;
        if(key == "Encoding")
            m_encoding = value;
    }
}
