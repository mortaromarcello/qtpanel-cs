#include "dockapplet.h"
#include <QDateTime>
#include <QTimer>
#include <QPainter>
#include <QFontMetrics>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QFile>
#include <QTextStream>
#include <QProcess>
#include <QDesktopServices>
#include "textgraphicsitem.h"
#include "panelapplication.h"
#include "panelwindow.h"
#include "animationutils.h"
#include "dpisupport.h"
#include "xfitman.h"
#include "procutils.h"
#include "client.h"

DockItem::DockItem(DockApplet* dockApplet, Client* client):
    m_dragging(false),
    m_highlightIntensity(0.0),
    m_urgencyHighlightIntensity(0.0),
    m_dockApplet(dockApplet),
    m_client(client),
    m_animationTimer(new QTimer()),
    m_textItem(new TextGraphicsItem(this)),
    m_iconItem(new QGraphicsPixmapItem(this))
{
    m_animationTimer->setInterval(20);
    m_animationTimer->setSingleShot(true);
    connect(m_animationTimer, SIGNAL(timeout()), this, SLOT(animate()));
    setParentItem(m_dockApplet);
    setAcceptsHoverEvents(true);
    setAcceptedMouseButtons(Qt::LeftButton | Qt::RightButton);
    m_textItem->setColor(Qt::white);
    m_textItem->setFont(m_dockApplet->panelWindow()->font());
}

DockItem::~DockItem()
{
    delete m_client;
    delete m_iconItem;
    delete m_textItem;
    delete m_animationTimer;
}

void DockItem::updateContent()
{
    QFontMetrics fontMetrics(m_textItem->font());
    QString shortName = fontMetrics.elidedText(m_client->name(), Qt::ElideRight, m_targetSize.width() - adjustHardcodedPixelSize(36));
    m_textItem->setText(shortName);
    m_textItem->setPos(adjustHardcodedPixelSize(28), m_dockApplet->panelWindow()->textBaseLine());
    m_iconItem->setPixmap(m_client->icon().pixmap(adjustHardcodedPixelSize(16)));
    m_iconItem->setPos(adjustHardcodedPixelSize(8), m_targetSize.height()/2 - adjustHardcodedPixelSize(8));
    update();
}

void DockItem::setTargetPosition(const QPoint& targetPosition)
{
    m_targetPosition = targetPosition;
    updateClientIconGeometry();
}

void DockItem::setTargetSize(const QSize& targetSize)
{
    m_targetSize = targetSize;
    updateClientIconGeometry();
    updateContent();
}

void DockItem::moveInstantly()
{
    m_position = m_targetPosition;
    m_size = m_targetSize;
    setPos(m_position.x(), m_position.y());
    update();
}

void DockItem::startAnimation()
{
    if(!m_animationTimer->isActive())
        m_animationTimer->start();
}

void DockItem::animate()
{
    bool needAnotherStep = false;
    static const qreal highlightAnimationSpeed = 0.15;
    qreal targetIntensity = isUnderMouse() ? 1.0 : 0.0;
    m_highlightIntensity = AnimationUtils::animate(m_highlightIntensity, targetIntensity, highlightAnimationSpeed, needAnotherStep);
    static const qreal urgencyHighlightAnimationSpeed = 0.015;
    qreal targetUrgencyIntensity = 0.0;
    if(isUrgent())
    {
        qint64 msecs = QDateTime::currentMSecsSinceEpoch() % 3000;
        if(msecs < 1500)
            targetUrgencyIntensity = 1.0;
        else
            targetUrgencyIntensity = 0.5;
        needAnotherStep = true;
    }
    m_urgencyHighlightIntensity = AnimationUtils::animate(m_urgencyHighlightIntensity, targetUrgencyIntensity, urgencyHighlightAnimationSpeed, needAnotherStep);
    if(!m_dragging)
    {
        static const int positionAnimationSpeed = 24;
        static const int sizeAnimationSpeed = 24;
        m_position.setX(AnimationUtils::animateExponentially(m_position.x(), m_targetPosition.x(), 0.2, positionAnimationSpeed, needAnotherStep));
        m_position.setY(AnimationUtils::animateExponentially(m_position.y(), m_targetPosition.y(), 0.2, positionAnimationSpeed, needAnotherStep));
        m_size.setWidth(AnimationUtils::animate(m_size.width(), m_targetSize.width(), sizeAnimationSpeed, needAnotherStep));
        m_size.setHeight(AnimationUtils::animate(m_size.height(), m_targetSize.height(), sizeAnimationSpeed, needAnotherStep));
        setPos(m_position.x(), m_position.y());
    }
    update();
    if(needAnotherStep)
        m_animationTimer->start();
}

void DockItem::close()
{
    //XfitMan::closeWindow(m_client->handle());
    m_client->close();
}

void DockItem::maximize()
{
    //XfitMan::maximizeWindow(m_client->handle(), XfitMan::MaximizeBoth);
    m_client->maximize();
}

void DockItem::demaximize()
{
    //XfitMan::deMaximizeWindow(m_client->handle());
    m_client->demaximize();
}

QRectF DockItem::boundingRect() const
{
    return QRectF(0.0, 0.0, m_size.width() - 1, m_size.height() - 1);
}

void DockItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    painter->setPen(Qt::NoPen);
    QPointF center(m_size.width()/2.0, m_size.height() + adjustHardcodedPixelSize(32));
    QRectF rect(0.0, adjustHardcodedPixelSize(4), m_size.width(), m_size.height() - adjustHardcodedPixelSize(8));
    static const qreal roundRadius = adjustHardcodedPixelSize(3);
    QRadialGradient gradient(center, adjustHardcodedPixelSize(200), center);
    gradient.setColorAt(0.0, QColor(255, 255, 255, 80 + static_cast<int>(80*m_highlightIntensity)));
    gradient.setColorAt(1.0, QColor(255, 255, 255, 0));
    painter->setBrush(QBrush(gradient));
    painter->drawRoundedRect(rect, roundRadius, roundRadius);
    if(m_urgencyHighlightIntensity > 0.001)
    {
        QRadialGradient gradient(center, adjustHardcodedPixelSize(200), center);
        gradient.setColorAt(0.0, QColor(255, 100, 0, static_cast<int>(160*m_urgencyHighlightIntensity)));
        gradient.setColorAt(1.0, QColor(255, 255, 255, 0));
        painter->setBrush(QBrush(gradient));
        painter->drawRoundedRect(rect, roundRadius, roundRadius);
    }
}

void DockItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
    startAnimation();
}

void DockItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
    startAnimation();
}

void DockItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if(event->button() == Qt::LeftButton)
    {
        m_dragging = true;
        m_mouseDownPosition = event->scenePos();
        m_dragStartPosition = m_position;
        m_dockApplet->draggingStarted();
        setZValue(1.0); // Be on top when dragging.
    }
}

void DockItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    if(event->button() == Qt::LeftButton)
    {
        m_dragging = false;
        m_dockApplet->draggingStopped();
        setZValue(0.0); // No more on top.
        startAnimation(); // Item can be out of it's regular, start animation to bring it back.
    }
    if(isUnderMouse())
    {
        if(event->button() == Qt::LeftButton)
        {
            static const qreal clickMouseMoveTolerance = 10.0;
            if((event->scenePos() - m_mouseDownPosition).manhattanLength() < clickMouseMoveTolerance)
            {
                if (m_client->isActive()) {
                    m_client->minimize();
                    MyDBG("mouseReleaseEvent:" << m_client->name() << "minimized");
                }
                else
                {
                    if (XfitMan::getActiveDesktop() != m_client->getDesktop())
                        XfitMan::setActiveDesktop(m_client->getDesktop());
                    m_client->activate();
                    MyDBG("mouseReleaseEvent:" << m_client->name() << "activate");
                }
                MyDBG("mouseReleaseEvent:" << m_client->name());
            }
        }
        if(event->button() == Qt::RightButton && !m_dragging)
        {
            QMenu menu;
            menu.addAction(QIcon::fromTheme("window-close"), tr("Close"), this, SLOT(close()));
            menu.addAction(QIcon::fromTheme("window_fullscreen"), tr("Maximize"), this, SLOT(maximize()));
            menu.addAction(QIcon::fromTheme("window_nofullscreen"), tr("Demaximize"), this, SLOT(demaximize()));
            for (int i = 0; i < XfitMan::getNumDesktop(); i++)
            {
                QAction* action = new QAction(QString(tr("Move to desktop %1")).arg(i+1), this);
                if (XfitMan::getWindowDesktop(m_client->handle()) == i)
                    action->setEnabled(false);
                menu.addAction(action);
            }
            QAction* result = menu.exec(event->screenPos());
            if (result) {
                for (int i = 0; i < XfitMan::getNumDesktop(); i++) {
                    if ((XfitMan::getActiveDesktop() != i) && (result->text() == QString(tr("Move to desktop %1")).arg(i+1))) {
                        XfitMan::moveWindowToDesktop(m_client->handle(), i);
                        XfitMan::setActiveDesktop(i);
                    }
                }
            }
        }
    }
}

void DockItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    // Mouse events are sent only when mouse button is pressed.
    if(!m_dragging)
        return;
    // TODO: Vertical orientation support.
    QPointF delta = event->scenePos() - m_mouseDownPosition;
    m_position.setX(m_dragStartPosition.x() + static_cast<int>(delta.x()));
    if(m_position.x() < 0)
        m_position.setX(0);
    if(m_position.x() >= m_dockApplet->size().width() - m_targetSize.width())
        m_position.setX(m_dockApplet->size().width() - m_targetSize.width());
    setPos(m_position.x(), m_position.y());
    int criticalShift = m_targetSize.width()*55/100;
    if(m_position.x() < m_targetPosition.x() - criticalShift)
        m_dockApplet->moveItem(this, false);
    if(m_position.x() > m_targetPosition.x() + criticalShift)
        m_dockApplet->moveItem(this, true);
    update();
}

void DockItem::updateClientIconGeometry()
{
    QPointF topLeft = m_dockApplet->mapToScene(m_targetPosition);
    QRect rect;
    rect.setX(static_cast<unsigned long>(topLeft.x()) + m_dockApplet->panelWindow()->pos().x());
    rect.setY(static_cast<unsigned long>(topLeft.y()) + m_dockApplet->panelWindow()->pos().y());
    rect.setWidth(m_targetSize.width());
    rect.setHeight(m_targetSize.height());
    XfitMan::setIconGeometry(m_client->handle(), &rect);
}

bool DockItem::isUrgent()
{
    if(m_client->isUrgent())
        return true;
    return false;
}
/*
Client::Client(unsigned long handle)
{
    m_handle = handle;
    m_pid = XfitMan::getWindowPid(handle);
    m_path = XfitMan::getExeForPid(m_pid);
    XfitMan::registerForWindowPropertyChanges(m_handle);
    std::list<std::string> cmdline = getCmdLineByPid(m_pid);
    for (std::string str: cmdline)
        m_cmdline << str.c_str();
    updateVisibility();
    updateName();
    updateIcon();
    updateUrgency();
    activate();
    int currentDesktop = XfitMan::getActiveDesktop();
    if (currentDesktop != getDesktop())
        XfitMan::setActiveDesktop(getDesktop());
}

Client::~Client()
{
}

pid_t Client::pid()
{
    XfitMan::getWindowPid(handle());
}

void Client::windowPropertyChanged(unsigned long atom)
{
    if(atom == XfitMan::atom("_NET_WM_WINDOW_TYPE") || atom == XfitMan::atom("_NET_WM_STATE")) {

        MyDBG(QString("atom:%1").arg(atom == XfitMan::atom("_NET_WM_WINDOW_TYPE")?"_NET_WM_WINDOW_TYPE":"_NET_WM_STATE"));

        updateVisibility();
    }
    if(atom == XfitMan::atom("_NET_WM_VISIBLE_NAME") || atom == XfitMan::atom("_NET_WM_NAME") || atom == XfitMan::atom("WM_NAME")) {
        updateName();

        MyDBG("atom:_NET_WM_VISIBLE_NAME || _NET_WM_NAME || WM_NAME ->" << name());

    }
    if(atom == XfitMan::atom("_NET_WM_ICON")) {

        MyDBG("atom:_NET_WM_ICON");

        updateIcon();
    }
    if(atom == XfitMan::atom("WM_HINTS")) {

    MyDBG("atom:WM_HINTS");

        updateUrgency();
    }
    if(atom == XfitMan::atom("_NET_WM_ACTIVE_WINDOW")) {

        MyDBG("atom:_NET_WM_ACTIVE_WINDOW");

    }
}

void Client::updateVisibility()
{
    AtomList windowTypes = XfitMan::getWindowType(handle());
    WindowState state = XfitMan::getWindowState(handle());
    m_visible = (windowTypes.size() == 0) || (windowTypes.size() == 1 && windowTypes[0] == XfitMan::atom("_NET_WM_WINDOW_TYPE_NORMAL"));
    if(state.SkipTaskBar)
        m_visible = false;
}

void Client::updateName()
{
    m_name = XfitMan::getWindowTitle(handle());
}

void Client::updateIcon()
{
    if (!XfitMan::getClientIcon(handle(), &m_icon))
        m_icon = QIcon();
}

void Client::updateUrgency()
{
    m_isUrgent = XfitMan::getWindowUrgency(handle());
}

bool Client::isActive()
{

    MyDBG("isActive:" << handle() << "->" << XfitMan::getActiveWindow());

    WindowState state = XfitMan::getWindowState(handle());
    MyDBG(state.Focused);
    return (handle() == XfitMan::getActiveWindow());
}

bool Client::isHidden()
{
    WindowState state;
    state = XfitMan::getWindowState(handle());
    return state.Hidden;
}

bool Client::isAboveLayer()
{
    WindowState state;
    state = XfitMan::getWindowState(handle());
    return state.AboveLayer;
}

int Client::getDesktop()
{
    return XfitMan::getWindowDesktop(handle());
}

void Client::activate()
{
    XfitMan::raiseWindow(handle());
}

void Client::minimize()
{
    XfitMan::minimizeWindow(handle());
}

void Client::maximize()
{
    XfitMan::maximizeWindow(handle(), XfitMan::MaximizeBoth);
}

void Client::demaximize()
{
    XfitMan::deMaximizeWindow(handle());
}

void Client::close()
{
    XfitMan::closeWindow(handle());
}
*/

DockApplet::DockApplet(PanelWindow* panelWindow)
    : Applet(panelWindow),
    m_dragging(false)
{
    // Register for notifications about window property changes.
    connect(XfitMan::instance(), SIGNAL(windowPropertyChanged(unsigned long,unsigned long)), this, SLOT(windowPropertyChanged(unsigned long,unsigned long)));
}

DockApplet::~DockApplet()
{
    m_dockItems.clear();
}

bool DockApplet::init()
{
    updateClientList();
    for(int i = 0; i < m_dockItems.size(); i++)
        m_dockItems[i]->moveInstantly();
    return true;
}

void DockApplet::updateLayout()
{
    // TODO: Vertical orientation support.
    int freeSpace = m_size.width();
    int spaceForOneClient = (m_dockItems.size() > 0) ? freeSpace/m_dockItems.size() : 0;
    int currentPosition = 0;
    for(int i = 0; i < m_dockItems.size(); i++)
    {
        int spaceForThisClient = spaceForOneClient;
        static const int maxSpace = adjustHardcodedPixelSize(256);
        if(spaceForThisClient > maxSpace)
            spaceForThisClient = maxSpace;
        m_dockItems[i]->setTargetPosition(QPoint(currentPosition, 0));
        m_dockItems[i]->setTargetSize(QSize(spaceForThisClient - 4, m_size.height()));
        m_dockItems[i]->startAnimation();
        currentPosition += spaceForThisClient;
    }
    update();
}

void DockApplet::draggingStarted()
{

    MyDBG("draggingStarted");

    m_dragging = true;
}

void DockApplet::draggingStopped()
{

    MyDBG("draggingStopped");

    m_dragging = false;
    // Since we don't update it when dragging, we should do it now.
}

void DockApplet::moveItem(DockItem* dockItem, bool right)
{
    int currentIndex = m_dockItems.indexOf(dockItem);
    if(right)
    {
        if(currentIndex != (m_dockItems.size() - 1))
        {
            m_dockItems.remove(currentIndex);
            m_dockItems.insert(currentIndex + 1, dockItem);
            updateLayout();
        }
    }
    else
    {
        if(currentIndex != 0)
        {
            m_dockItems.remove(currentIndex);
            m_dockItems.insert(currentIndex - 1, dockItem);
            updateLayout();
        }
    }
}

void DockApplet::xmlWrite(XmlConfigWriter* writer)
{
    writer->writeStartElement("config");
    writer->writeEndElement();
}

void DockApplet::layoutChanged()
{
    updateLayout();
}

QSize DockApplet::desiredSize()
{
    return QSize(-1, -1); // Take all available space.
}

void DockApplet::registerDockItem(DockItem* dockItem)
{
    m_dockItems.append(dockItem);
    updateLayout();
}

void DockApplet::unregisterDockItem(DockItem* dockItem)
{
    m_dockItems.remove(m_dockItems.indexOf(dockItem));
    delete dockItem;
    updateLayout();
}

void DockApplet::updateClientList()
{
    MyDBG("updateClientList");
    if(m_dragging)
        return;
    WindowList windows = XfitMan::getClientList();
    // Handle new clients.
    for(int i = 0; i < windows.size(); i++)
    {
        if (!getClientFromDockItems(windows[i]))
        {
            // Skip our own windows.
            if(QWidget::find(windows[i]))
                continue;
            Client* client = new Client(windows[i]);
            if (client->isVisible()) {
                registerDockItem(new DockItem(this, client));
                MyDBG("added client:" << client->name() << client->path());
            }
        }
    }
    // Handle removed clients.
    for(;;)
    {
        bool clientRemoved = false;
        for(DockItem* item: m_dockItems)
        {
            int handle = item->client()->handle();
            if(!windows.contains(handle))
            {

                MyDBG("removed client:" << item->client()->name());

                unregisterDockItem(item);
                clientRemoved = true;
                break;
            }
        }
        if(!clientRemoved)
            break;
    }
}

Client* DockApplet::getClientFromDockItems(unsigned long handle)
{
    DockItem* item;
    for (int i = 0; i < m_dockItems.size(); i++) {
        item = m_dockItems[i];
        if (item->client()->handle() == handle)
            return item->client();
    }
    return nullptr;
}

DockItem* DockApplet::getDockItem(unsigned long handle)
{
    DockItem* item;
    for (int i = 0; i < m_dockItems.size(); i++) {
        item = m_dockItems[i];
        if (item->client()->handle() == handle)
            return item;
    }
    return nullptr;
}

void DockApplet::windowPropertyChanged(unsigned long window, unsigned long atom)
{
    if(window == XfitMan::getRootWindow()) {
        if(atom == XfitMan::atom("_NET_CLIENT_LIST")) {

            MyDBG("atom:_NET_CLIENT_LIST" << atom);

            updateClientList();
        }
        if(atom == XfitMan::atom("_NET_ACTIVE_WINDOW")) {

            MyDBG("atom:_NET_ACTIVE_WINDOW" << atom);

        }
        return;
    }
    if(getClientFromDockItems(window) != nullptr) {
        getClientFromDockItems(window)->windowPropertyChanged(atom);
        if (atom == XfitMan::atom("_NET_WM_VISIBLE_NAME") || atom == XfitMan::atom("_NET_WM_NAME") || atom == XfitMan::atom("WM_NAME"))
            getDockItem(window)->updateContent();
    }
}
