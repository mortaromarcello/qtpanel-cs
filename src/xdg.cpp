#include "xdg.h"
#include <QProcessEnvironment>
#include <QDir>

XdgDirectory::XdgDirectory()
{
    m_environment = QProcessEnvironment::systemEnvironment();
    m_home = m_environment.value("HOME");
    if (! m_environment.contains("XDG_DATA_HOME"))
        m_xdg_data_home = m_home + QDir::separator() + XDG_DATA_HOME_DEFAULT;
    else
        m_xdg_data_home = m_environment.value("XDG_DATA_HOME");
    if (! m_environment.contains("XDG_CONFIG_HOME"))
        m_xdg_config_home = m_home + QDir::separator() + XDG_CONFIG_HOME_DEFAULT;
    else
        m_xdg_cache_home = m_environment.value("XDG_CONFIG_HOME");
    if (! m_environment.contains("XDG_CACHE_HOME"))
        m_xdg_cache_home = m_home + QDir::separator() + XDG_CACHE_HOME_DEFAULT;
    else
        m_xdg_cache_home = m_environment.value("XDG_CACHE_HOME");
    if (! m_environment.contains("XDG_DATA_DIRS"))
        m_xdg_data_dirs = XDG_DATA_DIRS_DEFAULT;
    else
        m_xdg_data_dirs = m_environment.value("XDG_DATA_DIRS");
    if (! m_environment.contains("XDG_CONFIG_DIRS"))
        m_xdg_config_dirs = XDG_CONFIG_DIRS_DEFAULT;
    else
        m_xdg_config_dirs = m_environment.value("XDG_CONFIG_DIRS");
    m_xdg_autostart_home = m_xdg_config_home + QDir::separator() + "autostart";
    QStringList autostart_dirs;
    for (QString dir: m_xdg_config_dirs.split(":"))
        autostart_dirs << dir + QDir::separator() + "autostart";
    m_xdg_autostart_dirs = autostart_dirs.join(":");
    m_xdg_trash_home = m_xdg_data_home + QDir::separator() + "Trash";
}

XdgDirectory::~XdgDirectory()
{}
