#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <list>
#include <cstring>
#include "procutils.h"

using namespace std;

pid_t getProcIdByName(const string& procName)
{
    int pid = -1;
    // Open the /proc directory
    DIR *dp = opendir("/proc");
    if (dp != NULL)
    {
        // Enumerate all entries in directory until process found
        struct dirent *dirp;
        while (pid < 0 && (dirp = readdir(dp)))
        {
            // Skip non-numeric entries
            int id = atoi(dirp->d_name);
            if (id > 0)
            {
                // Read contents of virtual /proc/{pid}/cmdline file
                string cmdPath = string("/proc/") + dirp->d_name + "/cmdline";
                ifstream cmdFile(cmdPath.c_str());
                string cmdLine;
                getline(cmdFile, cmdLine);
                if (!cmdLine.empty())
                {
                    // Keep first cmdline item which contains the program path
                    size_t pos = cmdLine.find('\0');
                    if (pos != string::npos)
                        cmdLine = cmdLine.substr(0, pos);
                    // Keep program name only, removing the path
                    pos = cmdLine.rfind('/');
                    if (pos != string::npos)
                        cmdLine = cmdLine.substr(pos + 1);
                    // Compare against requested process name
                    if (procName == cmdLine)
                        pid = id;
                }
            }
        }
    }
    closedir(dp);
    return pid;
}

list<string> getCmdLineByPid(pid_t pid)
{
    list<string> cmdList;
    char buff[128];
    snprintf(buff, sizeof(buff), "%d", pid);
    string cmdPath = string("/proc/") + buff + "/cmdline";
    ifstream cmdFile(cmdPath.c_str());
    string cmdLine;
    getline(cmdFile, cmdLine);
    size_t pos = cmdLine.find('\0');
    while (pos != string::npos)
    {
        cmdList.push_back(cmdLine.substr(0, pos));
        cmdLine.erase(0, pos + 1);
        pos = cmdLine.find('\0');
    }
    
    return cmdList;
}

#ifdef TEST

int main(int argc, char* argv[])
{
    // Fancy command line processing skipped for brevity
    pid_t pid = getProcIdByName(argv[1]);
    for (string str: getCmdLineByPid(pid))
        cout << "cmdline:" << str << endl;
    cout << "pid: " << pid << endl;
    return 0;
}
#endif
