#ifndef SENSORAPPLET_H
#define SENSORAPPLET_H

#include "applet.h"
#include "ui_appletsensorsettings.h"
#include "sensordata.h"
#include <QTimer>

class TextGraphicsItem;

class SensorApplet: public Applet
{
    CS_OBJECT(SensorApplet)
public:
    SensorApplet(PanelWindow* panelWindow);
    ~SensorApplet();
    bool init();
    void xmlWrite(XmlConfigWriter* writer);
    void showContextMenu(const QPoint& point);
    QSize desiredSize();

public :
    CS_SLOT_1(Public, void showConfigurationDialog())
    CS_SLOT_2(showConfigurationDialog) 
    CS_SLOT_1(Private, void updateInfo())
    CS_SLOT_2(updateInfo)
    CS_SLOT_1(Public, void readLabels(const QString &))
    CS_SLOT_2(readLabels)

protected:
    void layoutChanged();
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    bool xmlRead();
    void xmlReadChipset();
    void xmlReadLabel();
    void xmlReadColor();

private:
    QTimer m_timer;
    SensorsData m_data;
    SensorsChipName *m_chip;
    QString m_label;
    TextGraphicsItem* m_textItem;
    QColor m_color;
    Ui_AppletSensorSettings* m_settingsUi;
};

#endif // SENSORAPPLET_H
