#ifndef SESSIONAPPLET_H
#define SESSIONAPPLET_H

#include <QIcon>
#include <QMenu>
#include <QList>
#include <QStringList>
#include "applet.h"

class Ui_AppletSessionSettings;
class QGraphicsRectItem;
class Client;

class SessionApplet: public Applet
{
    CS_OBJECT(SessionApplet)
public:
    SessionApplet(PanelWindow* panelWindow);
    ~SessionApplet();
    bool init();
    void clicked();
    void xmlWrite(XmlConfigWriter* writer);
    void showContextMenu(const QPoint& point);
    QSize desiredSize();

public :
    CS_SLOT_1(Public, void showConfigurationDialog())
    CS_SLOT_2(showConfigurationDialog) 

protected:
    void layoutChanged();
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    QList<Client*> getListClientRunning();
    void closeApplications();
    void startApplication(QString name, QStringList args=QStringList());
    bool xmlRead();
    void xmlReadIcon();
    void xmlReadIconLogout();
    void xmlReadIconHalt();
    void xmlReadIconReboot();
    void xmlReadIconHibernate();
    void xmlReadIconSuspend();
    void xmlReadCommandLogout();
    void xmlReadCommandHalt();
    void xmlReadCommandReboot();
    void xmlReadCommandHibernate();
    void xmlReadCommandSuspend();
    void xmlReadColorText();
    void xmlReadColorBackground();
    void xmlReadColorSelectedText();
    void xmlReadColorSelectedBackground();
    void xmlReadColorBorder();
    void xmlReadRadiusBorderTopLeft();
    void xmlReadRadiusBorderTopRight();
    void xmlReadRadiusBorderBottomLeft();
    void xmlReadRadiusBorderBottomRight();
    void xmlReadHeightBorder();
    void xmlReadSaveSession();

private :
    CS_SLOT_1(Private, void logout())
    CS_SLOT_2(logout) 
    CS_SLOT_1(Private, void halt())
    CS_SLOT_2(halt) 
    CS_SLOT_1(Private, void reboot())
    CS_SLOT_2(reboot) 
    CS_SLOT_1(Private, void hibernate())
    CS_SLOT_2(hibernate) 
    CS_SLOT_1(Private, void suspend())
    CS_SLOT_2(suspend) 
    CS_SLOT_1(Private, void buttonIconClicked())
    CS_SLOT_2(buttonIconClicked) 
    CS_SLOT_1(Private, void buttonIconLogoutClicked())
    CS_SLOT_2(buttonIconLogoutClicked) 
    CS_SLOT_1(Private, void buttonIconRebootClicked())
    CS_SLOT_2(buttonIconRebootClicked) 
    CS_SLOT_1(Private, void buttonIconHaltClicked())
    CS_SLOT_2(buttonIconHaltClicked) 
    CS_SLOT_1(Private, void buttonIconHibernateClicked())
    CS_SLOT_2(buttonIconHibernateClicked) 
    CS_SLOT_1(Private, void buttonIconSuspendClicked())
    CS_SLOT_2(buttonIconSuspendClicked)
    CS_SLOT_1(Private, void saveSession())
    CS_SLOT_2(saveSession)

private:
    void createActions();
    void createMenu();
    QGraphicsPixmapItem* m_pixmapItem;
    bool m_menuOpened;
    QColor m_colorText;
    QColor m_colorBackground;
    QColor m_colorSelectedText;
    QColor m_colorSelectedBackground;
    QColor m_colorBorder;
    QIcon m_icon;
    QIcon m_iconLogout;
    QIcon m_iconHalt;
    QIcon m_iconReboot;
    QIcon m_iconHibernate;
    QIcon m_iconSuspend;
    QString m_commandLogout;
    QString m_commandHalt;
    QString m_commandReboot;
    QString m_commandHibernate;
    QString m_commandSuspend;
    QMenu* m_menu;
    QAction* m_logout;
    QAction* m_halt;
    QAction* m_reboot;
    QAction* m_hibernate;
    QAction* m_suspend;
    QAction* m_separator1;
    QAction* m_separator2;
    QAction* m_actionSaveSession;
    int m_radiusBorderTopLeft;
    int m_radiusBorderTopRight;
    int m_radiusBorderBottomLeft;
    int m_radiusBorderBottomRight;
    int m_heightBorder;
    bool m_saveSession;
    Ui_AppletSessionSettings* m_settingsUi;
};

#endif // SESSIONAPPLET_H
