#ifndef DIALOGAPPLETOPTIONS_H
#define DIALOGAPPLETOPTIONS_H

#include "applet.h"
#include <QDialog>

class QListWidget;
class PanelWindow;

class DialogAppletOptions: public QDialog
{
	CS_OBJECT(DialogAppletOptions)
public:
	DialogAppletOptions(PanelWindow *panelWindow, QVector<Applet*> applets, QWidget* parent = 0);
	~DialogAppletOptions();
	QVector <Applet*> getApplets() {return m_applets;};
public :
	CS_SLOT_1(Public, void upButtonClick())
	CS_SLOT_2(upButtonClick) 
	CS_SLOT_1(Public, void downButtonClick())
	CS_SLOT_2(downButtonClick) 
	CS_SLOT_1(Public, void addButtonClick())
	CS_SLOT_2(addButtonClick) 
	CS_SLOT_1(Public, void deleteButtonClick())
	CS_SLOT_2(deleteButtonClick) 
	CS_SLOT_1(Public, void configureButtonClick())
	CS_SLOT_2(configureButtonClick) 
private:
	PanelWindow* m_panelWindow;
	QVector<Applet*> m_applets;
	QListWidget* m_listWidget;
};

#endif // DIALOGAPPLETOPTIONS_H
