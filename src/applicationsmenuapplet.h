#ifndef APPLICATIONSMENUAPPLET_H
#define APPLICATIONSMENUAPPLET_H

#include <QList>
#include <QMap>
#include <QAction>
#include <QIcon>
#include <QCleanlooksStyle>
#include "applet.h"

class Ui_AppletApplicationsMenuSettings;

class ApplicationsMenuStyle: public QCleanlooksStyle
{
    CS_OBJECT(ApplicationsMenuStyle)
public:
    int pixelMetric(PixelMetric metric, const QStyleOption* option, const QWidget* widget) const;
};

class SubMenu
{
public:
    SubMenu(): m_menu(nullptr)
    {
    }

    SubMenu(QMenu* parent, const QString& title, const QString& category, const QString& icon);

    QMenu* menu()
    {
        return m_menu;
    }

    const QString& category() const
    {
        return m_category;
    }

private:
    QMenu* m_menu;
    QString m_category;
    QIcon m_icon;
};

class TextGraphicsItem;
class DesktopApplication;

class ApplicationsMenuApplet: public Applet
{
    CS_OBJECT(ApplicationsMenuApplet)
public:
    ApplicationsMenuApplet(PanelWindow* panelWindow);
    ~ApplicationsMenuApplet();

    bool init();
    QSize desiredSize();
    void clicked();
    void xmlWrite(XmlConfigWriter* writer);
    void showContextMenu(const QPoint& point);

public :
    CS_SLOT_1(Public, void showConfigurationDialog())
    CS_SLOT_2(showConfigurationDialog) 

protected:
    void layoutChanged();
    bool isHighlighted();
    bool xmlRead();
    void xmlReadMenuIcon();
    void xmlReadOpacity();
    void xmlReadBackgroundMenuColor();
    void xmlReadTextMenuColor();
    void xmlReadSelectedMenuColor();
    void xmlReadBackgroundSelectedMenuColor();
    void xmlReadMenuBorderRadiusTopLeft();
    void xmlReadMenuBorderRadiusTopRight();
    void xmlReadMenuBorderRadiusBottomLeft();
    void xmlReadMenuBorderRadiusBottomRight();
    void xmlReadMenuBorderWidth();
    void xmlReadMenuBorderColor();
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    void setOpacity(qreal opacity);

private :
    CS_SLOT_1(Private, void actionTriggered())
    CS_SLOT_2(actionTriggered) 
    CS_SLOT_1(Private, void applicationUpdated(const DesktopApplication & app))
    CS_SLOT_2(applicationUpdated) 
    CS_SLOT_1(Private, void applicationRemoved(const QString & path))
    CS_SLOT_2(applicationRemoved) 
    CS_SLOT_1(Private, void buttonIconClicked())
    CS_SLOT_2(buttonIconClicked) 
    CS_SLOT_1(Private, void updateQtpanelFromGit())
    CS_SLOT_2(updateQtpanelFromGit) 

private:
    ApplicationsMenuStyle m_style;
    TextGraphicsItem* m_textItem;
    bool m_menuOpened;
    QMenu* m_menu;
    QList<SubMenu> m_subMenus;
    QMap<QString, QAction*> m_actions;
    //
    QGraphicsPixmapItem* m_pixmapItem;
    QIcon m_icon;
    qreal m_opacity;
    QColor m_backgroundMenuColor;
    QColor m_textMenuColor;
    QColor m_selectedMenuColor;
    QColor m_backgroundSelectedMenuColor;
    QColor m_menuBorderColor;
    QString m_menuStyleSheet;
    int m_menuBorderWidth;
    int m_menuBorderRadiusTopLeft;
    int m_menuBorderRadiusTopRight;
    int m_menuBorderRadiusBottomLeft;
    int m_menuBorderRadiusBottomRight;
    Ui_AppletApplicationsMenuSettings *m_settingsUi;
};

#endif
