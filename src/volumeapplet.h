#ifndef VOLUMEAPPLET_H
#define VOLUMEAPPLET_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QIcon>
#include <QSlider>
#include <alsa/asoundlib.h>
#include "applet.h"

class Ui_AppletVolumeSettings;

class VolumeApplet: public Applet
{
    CS_OBJECT(VolumeApplet)
public:
    VolumeApplet(PanelWindow* panelWindow);
    ~VolumeApplet();

    bool init();
    QSize desiredSize();
    void clicked();
    void showContextMenu(const QPoint& point);

public :
    CS_SLOT_1(Public, void showConfigurationDialog())
    CS_SLOT_2(showConfigurationDialog) 
    CS_SLOT_1(Public, void valueChangedSlot(int value))
    CS_SLOT_2(valueChangedSlot) 
    CS_SLOT_1(Public, void launchMixer())
    CS_SLOT_2(launchMixer) 
    CS_SLOT_1(Public, void buttonIconClicked())
    CS_SLOT_2(buttonIconClicked) 
    CS_SLOT_1(Public, void mixerChanged(const QString& mixer))
    CS_SLOT_2(mixerChanged) 

protected:
    void layoutChanged();
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    bool asoundInitialize();
    bool asoundFindElement(const char * ename);
    void updateIcon();
    bool xmlRead();
    void xmlReadMixer();
    void xmlReadIconMixer();
    void xmlReadVolume();
    void xmlWrite(XmlConfigWriter* writer);

private:
    void setIcon(QGraphicsPixmapItem* item, const QString &icon, QIcon::Mode mode = QIcon::Normal, int extent = 24, const QString &ext = "");
    QGraphicsPixmapItem* m_pixmapItem;
    int m_delta;
    QSlider* m_slider;
    snd_mixer_t* m_handle;
    snd_mixer_elem_t* m_elem;
    snd_mixer_selem_id_t* m_sid;
    QString m_mixer;
    QIcon m_icon_mixer;
    Ui_AppletVolumeSettings* m_settingsUi;
    bool m_useInternalIcon;
    long m_volume;
};

#endif //VOLUMEAPPLET_H
