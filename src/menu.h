#ifndef MENU_H
#define MENU_H

#include <QString>
#include <QList>
#include <QDir>

class QMenu;

class Directory
{
public:
    Directory() {}
    Directory(QString filename);
    ~Directory() {}
    static bool exists(QString filename) {return QDir(filename).exists();}
    const QString& filename() const {return m_filename;}
    const QString& name() const {return m_name;}
    const QString& comment() const {return m_comment;}
    const QString& type() const {return m_type;}
    const QString& encoding() const {return m_encoding;}
private:
    QString m_filename;
    QString m_name;
    QString m_comment;
    QString m_icon;
    QString m_type;
    QString m_encoding;
};

class Menu
{
public:
    Menu(bool is_root=false, Menu *parent=nullptr, QString app_dir="", QString default_app_dirs="", QString directory_dir="",
            QString default_directory_dirs="", QString name="", Directory directory=Directory(),
            QString filename="", QString category="");
    ~Menu()
        {}
    const bool isRootMenu() const {return m_isRootMenu;}
    const QString& appDir() const {return m_appDir;}
    const QString& defaultAppDirs() const {return m_defaultAppDirs;}
    const QString& directoryDir() const {return m_directoryDir;}
    const QString& defaultDirectoryDir() const {return m_defaultDirectoryDirs;}
    const QString& name() const {return m_name;}
    const Directory& directory() const {return m_directory;}
    const QString& filename() const {return m_filename;}
    const QString& category() const {return m_category;}
    const QList<Menu*> subMenus() const {return m_subMenus;}
    Menu* parent() {return m_parent;}
    void addMenu(Menu* menu)
        {if(menu->parent()) m_subMenus.append(menu);}
    void print();
    Menu* getRootMenu();

private:
    bool m_isRootMenu;
    QString m_appDir;
    QString m_defaultAppDirs;
    QString m_directoryDir;
    QString m_defaultDirectoryDirs;
    QString m_name;
    Directory m_directory;
    QString m_filename;
    QString m_category;
    QList<Menu*> m_subMenus;
    Menu* m_parent;
};

Menu* createMenu(QString filename);

/*
class SubMenu
{
public:
    SubMenu(): m_menu(nullptr)
    {
    }

    SubMenu(QMenu* parent, const QString& title, const QString& category, const QString& icon);

    QMenu* menu()
    {
        return m_menu;
    }

    const QString& category() const
    {
        return m_category;
    }

private:
    QMenu* m_menu;
    QString m_category;
    QIcon m_icon;
};
*/

#endif // MENU_H
