# - Find the Xxf86vm include file and library
#

SET(CopperSpice_INC_SEARCH_PATH
    /usr/local/include
    /usr/local/include/phonon
    /usr/local/include/QtCore
    /usr/local/include/QtMultimedia
    /usr/local/include/QtOpenGL
    /usr/local/include/QtSql
    /usr/local/include/QtWebKit
    /usr/local/include/QtXmlPatterns
    /usr/local/include/QtGui
    /usr/local/include/QtNetwork
    /usr/local/include/QtScript
    /usr/local/include/QtSvg
    /usr/local/include/QtXml)

SET(CopperSpice_LIB_SEARCH_PATH
    /usr/local/lib)


FIND_PATH(CopperSpice_INCLUDE_DIR qt-acconfig.h ${CopperSpice_INC_SEARCH_PATH})
FIND_PATH(CopperSpice_CORE_INCLUDE_DIR cs_build_info.h ${CopperSpice_INC_SEARCH_PATH})
FIND_PATH(CopperSpice_GUI_INCLUDE_DIR cs_carbon_wrapper.h ${CopperSpice_INC_SEARCH_PATH})
FIND_PATH(CopperSpice_PHONON_INCLUDE_DIR abstractaudiooutput.h ${CopperSpice_INC_SEARCH_PATH})
FIND_PATH(CopperSpice_SQL_INCLUDE_DIR qdb2driver.h ${CopperSpice_INC_SEARCH_PATH})

FIND_LIBRARY(CopperSpice_CORE_LIBRARY NAME CsCore1.4 PATHS ${CopperSpice_LIB_SEARCH_PATH})

FIND_LIBRARY(CopperSpice_GUI_LIBRARY NAME CsGui1.4 PATHS ${CopperSpice_LIB_SEARCH_PATH})

FIND_LIBRARY(CopperSpice_SQL_LIBRARY NAME CsSql1.4 PATHS ${CopperSpice_LIB_SEARCH_PATH})

FIND_LIBRARY(CopperSpice_MULTIMEDIA_LIBRARY NAME CsMultimedia1.4 PATHS ${CopperSpice_LIB_SEARCH_PATH})

FIND_LIBRARY(CopperSpice_PHONON_LIBRARY NAME CsPhonon1.4 PATHS ${CopperSpice_LIB_SEARCH_PATH})

IF (CopperSpice_INCLUDE_DIR AND CopperSpice_CORE_LIBRARY)
    SET(CopperSpice_FOUND TRUE)
ENDIF (CopperSpice_INCLUDE_DIR AND CopperSpice_CORE_LIBRARY)

IF (CopperSpice_FOUND)
    INCLUDE(CheckLibraryExists)

    CHECK_LIBRARY_EXISTS(${CopperSpice_CORE_LIBRARY}
                         "qt_application_thread_id"
                         CsCore1.4
                         CopperSpice_HAS_CONFIG)

    IF (NOT CopperSpice_HAS_CONFIG AND CopperSpice_FIND_REQUIRED)
        MESSAGE(FATAL_ERROR "Could NOT find CopperSpice")
    ENDIF (NOT CopperSpice_HAS_CONFIG AND CopperSpice_FIND_REQUIRED)
ENDIF (CopperSpice_FOUND)

#MARK_AS_ADVANCED(
#    CopperSpice_INCLUDE_DIR
#    CopperSpice_CORE_LIBRARY
#    CopperSpice_GUI_LIBRARY
#    CopperSpice_PHONON_LIBRARY
#    )
